function maj_liste_acteur(event) {
    // debugger;
    
    let selection = document.querySelector("#action").selectedIndex;
    let acteurs	= document.querySelector("#acteurs");

    let correcteurs = document.querySelectorAll("#acteurs .correcteurs");
    let interviewers = document.querySelectorAll("#acteurs .interviewers");

    if (selection === 0){
	acteurs.classList.add("hide");
    }else{
	acteurs.classList.remove("hide");
    }

    if (event.value === "correction"){
	// on cache les interviewers
	set_class(interviewers, 'hide');
	// on affiche les correcteurs
	unset_class(correcteurs, 'hide');
    } else if (event.value === "entretien"){
	set_class(correcteurs, 'hide');
	unset_class(interviewers, 'hide');
    }
}

function exec_attribution(event){
    if ((document.querySelector("#action").value === "") ||
	(document.querySelector("#acteurs").value === "") ||
	(get_checked_checkboxes.length === null)){
	debugger;
	window.alert("Vous devez choisir une action et un conseiller et au moins un dossier pour faire son attribution !");
    }else{
	window.location.href = document.URL;
    }
}

function get_checked_checkboxes(){
    return document.querySelectorAll("input[type=checkbox]:checked");
}
// function httpGetAsync(theUrl, callback)
// {
//     var xmlHttp = new XMLHttpRequest();
//     xmlHttp.onreadystatechange = function() { 
//         if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
//             callback(xmlHttp.responseText);
//     };
//     xmlHttp.open("GET", theUrl, true); // true for asynchronous 
//     xmlHttp.send(null);
// }

function toggle_class(nodelist, classes) {
    // Toggle les classes des élements de la nodeliste
    // classes peut être une liste de classes mais en string "classA classB classC"
    if (typeof(classes) === "string") {
	classes = classes.trim().split(" ");
    }
    let n = nodelist.length;
    let p = classes.length;

    for (let j=0; j<p; j++){
	let classe = classes[j];
	for (let i=0; i<n; i++){
	    nodelist[i].classList.toggle(classe);
	}
	return nodelist;
    }
}

function set_class(nodelist, classes){
    // unset la classe au nodeliste, 
    let n = nodelist.length;
    for (let i=0; i<n; i++){
	nodelist[i].classList.remove(classes);
    }
    return nodelist;    
}


function unset_class(nodelist, classes){
    // unset la classe au nodeliste
    let n = nodelist.length;
    for (let i=0; i<n; i++){
	nodelist[i].classList.add(classes);
    }
    return nodelist;    
}

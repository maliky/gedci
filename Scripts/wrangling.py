import numpy as np
import pandas as pd
import logging
from pandas.errors import OutOfBoundsDatetime
from gedci.gedcilib import arguments
from gedci.gedcilib.utils import get_latest_export_basename


class GEDCIWrangler():

    def __init__(self, fname=None, dirname='./Data'):
        if fname is None or fname == '':
            self.ffname = '.' + get_latest_export_basename(dirname)
        else:
            self.ffname = f"{dirname}/{fname}"

        self.df = pd.DataFrame()
        self.colnames = ['N° de dossier', 'Type de dossier', 'Prénom', 'Nom', 'Adresse mail', 'Date de naissance', 'Dernier établissement du dossier académique', 'Nom groupement – établissement choisi', 'Niveau d’études atteint', 'Niveau d’étude visé', 'Etat du dossier', "Etat de l’entretien", 'Etat de paiement', 'Date de première soumission', 'Date de dernière soumission du dossier', 'Date de validation du dossier', "Date de l’entretien", 'Date de validation du paiement']
        self.datetimecols = ['Date de première soumission', 'Date de dernière soumission du dossier', 'Date de validation du dossier', "Date de l’entretien", 'Date de validation du paiement']

    def load_file(self):
        self.df = pd.read_excel(f"{self.ffname}.xls", header=0, skiprows=3, dtype=str)
        self.df.index.name = "gedci"

    def replace_nans(self):
        self.df = self.df.replace(to_replace='nan', value='')

    def date_naiss_conversion(self, colnames=['Date de naissance']):
        for coln in colnames:
            self.df.loc[:, coln] = self.df.loc[:, coln].apply(date_conv)

    def datetime_conversion(self):
        for coln in self.datetimecols:
            self.df.loc[:, coln] = self.df.loc[:, coln].apply(datetime_conv)

    def save(self):
        self.df.loc[:, self.colnames].to_csv(f"{self.ffname}.csv")

    def run(self):
        self.load_file()
        logging.info(f"{self.ffname}.xls loaded!")
        
        self.replace_nans()
        logging.info("Replacement done!")
        
        self.date_naiss_conversion()
        logging.info("convertion date naissance done!")
        
        self.datetime_conversion()
        logging.info("convertion des datetime done!")
        
        self.save()
        logging.info(f"Sauvegarde dans {self.ffname}.csv done!")


# #### Divers ####
def date_conv(dn):
    """Soit dn une date au format string, vérifie des erreurs et renvois la date au format %Y-%d-%m"""
    day, month, year = map(int, dn.split('/'))
    year = 1900 if (1930 > year) or (year > (pd.Timestamp.now().year - 10)) else year
    month = 1 if month > 12 else month
    daysinmonth = pd.Timestamp(year=year, month=month, day=1).daysinmonth
    day = 1 if day > daysinmonth else day

    ts = ''
    try:
        ts = pd.Timestamp(year=year, month=month, day=day).strftime("%Y-%m-%d")
    except OutOfBoundsDatetime as obd:
        logging.error(f"pb avec {dn} dans {obd}")
        raise obd

    return ts


def datetime_conv(dt):
    """Soit une Datetime dt la converti en Y-m-d"""
    try:
        if isinstance(dt, str) and (dt != ''):
            ts = pd.Timestamp(dt).round('ms')
            return ts.strftime("%Y-%m-%d %H:%M")
        # else:
            # ts = pd.Timestamp('1900-01-01')
            # return ts.strftime("%Y-%m-%d %H:%M")  # or ''
    except ValueError as ve:
        logging.warning("dt of type %s = '%s'" % (type(dt), dt))
        raise



def get_unique_col_entries(df, k=0):
    """Given a df, returns a serie avec en index les noms des colonnes et en valeurs une série de values count si un des value count représente plus de k*len(df)"""
    D = {}
    for colname in df.columns:
        colsr = df.loc[:, colname]  # column serie
        vc = colsr.value_counts()  # value count
        if vc.where(vc.values >= len(colsr) * k).any():
            # true if some values count > than threshold
            D[colname] = df.loc[:, colname].value_counts().to_dict()
        else:
            pass
        sr = pd.Series(D)
        sr.index.name = df.index.name
    return sr


# #### Script ####

# voir get arguments from command ligne in kola
args = arguments.get_wrangle_args()

if __name__ == '__main__':
    try:
        dirname = args.download_dir  # './Data'
        fname = args.file_name
        logging.getLogger("").setLevel(args.log_level)
    except Exception as e:
        raise e

    try:
        gedciWrangler = GEDCIWrangler(fname, dirname)
        gedciWrangler.run()
    except KeyboardInterrupt as e:
        logging.warning('ERROR: %s.' % e)

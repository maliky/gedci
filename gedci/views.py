from django.shortcuts import render, redirect
from gedci.models import UserGEDCI, Attribution, Dossier, Importation, EtatAtt, AbstractEtat
from gedci.forms import ImportationForm, DossierForm
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
import logging
from django.views import generic
from gedci.gedcilib import func4views as v
from gedci.gedcilib import settings as S

# ############### Main view classes ################

class Accueil(LoginRequiredMixin, View):

    def get(self, request):
        # J'arrive ici que si mon user est identifié
        logging.info(f"GET {request.user.groups}")

        if len(request.user.groups.filter(name__in=S.GROUPS_CHEF)):
            return redirect('/cdp/soumis/doc')

        elif len(request.user.groups.filter(name__in=S.GROUPS_CONSEIL)):
            return redirect(f'/conseillers/tous/doc/{request.user.id}')


class Chefs2Pole(LoginRequiredMixin, View):
    """Gére les manip avant l'affichage de l'accueil. Si new alors fait un nouvel accueil."""

    def post(self, request, order="numDos", filtre="tous"):

        try:
            acteurid = request.POST['acteurs'].split('-')[1]
            # on met à jour les attribution
            _ = v.set_attributions(request, acteurid=acteurid,
                                   docIds=v.get_checkbox_id(request))
        except IndexError as ie:
            pass
            
        return self.get(request, order, filtre)
        
    def get(self, request, order="numDos", filtre="tous"):

        # on récupère les dossiers
        context = v.get_Dos_context(order=order,
                                    filtre=filtre)

        # pagination
        context['dataset'] = v.set_pagination(request=request,
                                              dataset=context['dataset'],
                                              default_page=1,
                                              max_page=100)

        # on ajoute le context pour le menu des chefs de poles
        context.update(v.cdp_menu_context())

        return render(request, 'gedci/cdp.html', context)


class Stats(LoginRequiredMixin, View):
    def get(self, request):
        try:
            importation = Importation.objects.all().latest('dateCrea')
        except Exception as e:
            # devrait toujours en avoir une même nulle
            raise e
        # importForm = ImportationForm()
        cols = ["dateCrea", "precedente", "nom_fichier_import", "date_fichier_import", "nb_dossiers", "nb_nouveaux_docs", "nb_updated_docs", "nb_paiements_valides", "nb_visa_renseignes", "nb_scac_instruit", "nb_favorables", "nb_defavorables", "nb_entretenus2", "nb_soumissions", "nb_validations", "nb_entretiens_autorises", "nb_entretiens_valides", "nb_entretiens_demandes", "nb_entretiens_planifie", "nb_entretiens_annules", ]
        importForm = {k.replace("_", " "):v for (k,v) in importation.__dict__.items() if k in cols}

        return render(request, 'gedci/stats.html', context={'imports': importForm.items()})


class ImportationView(LoginRequiredMixin, View):

    def get(self, request):
        context = {}
        context['dataset'] = Importation.objects.order_by('-dateCrea')
        context['order'] = 'doc'
        context.update(v.cdp_menu_context())

        context['dataset'] = v.set_pagination(request=request,
                                              dataset=context['dataset'],
                                              default_page=1,
                                              max_page=1)

        return render(request, 'gedci/importations.html', context=context)
    
class Conseillers2(LoginRequiredMixin, View):

    def post(self, request, filtre, order, acteurid):

        acteur = UserGEDCI.objects.get(pk=acteurid)
        docIds = v.get_checkbox_id(request)

        _ = v.maj_attributions(acteur=acteur,
                               docIds=docIds,
                               etatMaj=request.POST['action'])

        return self.get(request, filtre, order, acteurid)
    
    def get(self, request, filtre, order, acteurid):

        acteur = v.get_model_instance(UserGEDCI, acteurid)
        
        etatAttChoix = set(EtatAtt.CHOIX) - set(AbstractEtat.CHOIX)

        filtre = 'tous' if filtre is None else filtre
        order = 'doc' if order is None else order
        
        context = {"actions": etatAttChoix,
                   "dataset": v.get_docAtt(acteur, order, filtre),
                   "filtre": filtre,
                   "order": order,
                   "acteur": acteur,
                   "acteurid": acteurid}

        context.update(v.cdp_menu_context())
        # pagination
        context['dataset'] = v.set_pagination(request=request,
                                              dataset=context['dataset'],
                                              default_page=1,
                                              max_page=100)

        return render(request, 'gedci/conseillers2.html', context=context)

    

class Conseillers(LoginRequiredMixin, View):

    def post(self, request, filtre, order, acteur):

        acteur = UserGEDCI.objects.get(pk=request.user.pk)
        docIds = v.get_checkbox_id(request)

        _ = v.maj_attributions(acteur=acteur,
                               docIds=docIds,
                               etatMaj=request.POST['action'])

        return self.get(request, filtre, order, acteur)
    
    def get(self, request, filtre, order, acteur):

        acteur = request.user
        etatAttChoix = set(EtatAtt.CHOIX) - set(AbstractEtat.CHOIX)

        filtre = 'tous' if filtre is None else filtre
        order = 'doc' if order is None else order
        
        context = {"actions": etatAttChoix,
                   "dataset": v.get_docAtt(acteur, order, filtre),
                   "filtre": filtre,
                   "order": order,
                   "acteur": acteur}

        # pagination
        context['dataset'] = v.set_pagination(request=request,
                                              dataset=context['dataset'],
                                              default_page=1,
                                              max_page=100)

        return render(request, 'gedci/conseillers.html', context=context)


class EtatsDosView(LoginRequiredMixin, View):

    def get(self, request, docid):
        """Renvois une liste des états du doc"""
        doc = v.get_model_instance(Dossier, docid)
        etatsdocs = list(set(doc.etatsDoc.all()))
        doc._update_DocStat()

        context = v.get_etatsdoc_context(doc, etatsdocs)

        # pagination
        context['dataset'] = v.set_pagination(request=request,
                                              dataset=context['dataset'],
                                              default_page=1,
                                              max_page=100)

        return self.renvois_template(request, context)

    
    def renvois_template(self, request, context):
        if v.est_dans_group(request.user, gname="chef"):
            context.update(v.cdp_menu_context())
            return render(request, 'gedci/detail_etatDos_cdp.html', context=context)
        else:
            context['acteur'] = request.user
            return render(request, 'gedci/detail_etatDos.html', context=context)



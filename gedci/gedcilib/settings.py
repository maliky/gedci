from gedci import models as m
KORCOLSD = {
    'typeDos': 'Type de dossier',
    'prenomEtu': 'Prénom',
    'nomEtu': 'Nom',
    'emailEtu': 'Adresse mail',
    'dateNaisEtu': 'Date de naissance',
    'etabO': 'Dernier établissement du dossier académique',
    'etabV': 'Nom groupement – établissement choisi',
    'nivoAtteint': 'Niveau d’études atteint',
    'nivoVise': 'Niveau d’étude visé'
}

# ################ LES ETATS ################

# #### AbstractEtat
NONCREE = None
VALIDE = "Validé"
INDIFFERENT = "Indifférent"
NONAUTORISE ="Non autorisé"

# #### EtatAtt
FAITE = "Fait(e)"
ENCOURS = "En cours"

# #### EtatD
DEFAVORABLE = "Défavorable"
ENSAISIE = "En saisie étudiant"
ENTRETENU = "Synthèse d'entretien terminée"
FAVORABLE = "Favorable"
INSTRUIT = "Instruction SCAC terminée"
SOUMIS = "Soumis"
VISA = "Décision Visa Renseignée"

# #### EtatPaiement & EtatRDV
AUTORISE = "Autorisé"
DEMANDE = "Demandé"
PASDEMANDE = "Pas demandé"
PLANIFIE = "Planifié"

# #### Spécifique EtatPaiement
DEMANDEDANNULATION = "Demande d'annulation"

# #### Spécifique EtatRDV
DECLARE = "Déclaré"

# ################ Attribution ################

CORRECTION = 'Correction'
ENTRETIEN = 'Entretien'


# ################ INTERFACE AVEC LA db ################

ETATCOLSD = {"etatD": 'Etat du dossier',
             "etatP": 'Etat de paiement',
             "etatR": "Etat de l’entretien",
             "dateFSoum": 'Date de première soumission',
             "dateLSoum": 'Date de dernière soumission du dossier',
             "dateVDos": 'Date de validation du dossier',
             "dateEnt": "Date de l’entretien",
             "datePai": 'Date de validation du paiement'}

SORT_ORDERS = {'typed': "typeDos",
               '-typed': "-typeDos",
               "doc": "numDos",
               "-doc": "-numDos",
               "nom": "nomEtu",
               "-nom": "-nomEtu",
               "etat": "etat",
               "-etat": "-etat",
               "etatAtt": "etatAtt",
               "-etatAtt": "-etatAtt",
               "att": "attrib",
               "-att": "-attrib"}


GROUPS_CHEF = ['chefs_de_poles', 'admin']
GROUPS_CONSEIL = ['correcteurs', 'interviewers']


# #### FILTRES ####

FILTRE_ETATD = {"def": DEFAVORABLE,
                "ensaisie": ENSAISIE,
                "entretenu": ENTRETENU,
                "favorable": FAVORABLE,
                "instruit": INSTRUIT,
                "soumis": SOUMIS,
                "visa": VISA,}

# FILTRE_ETATD = {"def": m.EtatD.DEFAVORABLE,
#                 "ensaisie": m.EtatD.ENSAISIE,
#                 "entretien": m.EtatD.ENTRETENU,
#                 "favorable": m.EtatD.FAVORABLE,
#                 "instruit": m.EtatD.INSTRUIT,
#                 "soumis": m.EtatD.SOUMIS,
#                 "visa": m.EtatD.VISA,}


FILTRES = {'tous': 'tous',
           **FILTRE_ETATD}

FILTRE_ATTRIB = {"encours": ENCOURS,
                 "faite": FAITE}




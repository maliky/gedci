from django.contrib import admin
from gedci.models import UserGEDCI
from django.contrib.auth.admin import UserAdmin

@admin.register(UserGEDCI)
class UserAdmin(UserAdmin):
    pass


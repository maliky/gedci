from .scraptools import find_elt_by, find_elts_css
import pandas as pd

def check(elt):
    """active un chekbox non activé et sinon le laisse activé"""
    if not elt.get_attribute('checked'):
        elt.click()

def decheck(elt):
    """active un chekbox non activé et sinon le laisse activé"""
    if elt.get_attribute('checked'):
        elt.click()


def set_radio(i, list):
    """given a list an an indice set the ieme element of that list"""
    elts = list.find_elements_by_tag_name('label')
    assert i < len(elts)
    elts[i].click()

def get_selections(d):
    """retourne un dict avec les noms et les selections ( choix avec liste déroulante)"""
    D = {}
    for e in find_elts_css(d, 'select'):
        name = e.get_attribute('id')
        if name:
            D[name] = e

    return D

def get_options(d, idselect):
    """Renvoie une série avec les options et leur valeurs respective dans la liste d'options
idliste est un string id du tag select"""
    elts = find_elt_by('ID', d, idselect)
    D = {}
    for elt in elts.find_elements_by_tag_name('option'):
        D[elt.get_attribute('value')] = elt.text

    s = pd.Series(D)
    s.name = 'idselect'
    s.index.name = 'value'
    return s

def set_option(i, list, d=None):
    """given a list an an indice set the ieme element of that list
    la liste peut un in string si d est donnée"""
    if d:
        sels = get_selections(d)
        list=sels[list]
        elts = list.find_elements_by_tag_name('option')
        assert i < len(elts)
        elts[i].click()


# #### Remplissage du formulaire de recherche ####
def remplir_formulaire_recherche(d, default=True):
    """Fait un remplissage par defaut du formulaire"""
    if default:
        check(find_elt_by('ID', d, 'choixDossierCandidature'))
        decheck(find_elt_by('ID', d, 'choixRefusCandidatException'))
        check(find_elt_by('ID', d, 'choixDossierPreconsulaire')) 
        check(find_elt_by('ID', d, 'choixAcceptationElectronique'))
        check(find_elt_by('ID', d, 'choixProgrammeEchange'))
        check(find_elt_by('ID', d, 'choixAcceptationPapier'))
        decheck(find_elt_by('ID', d, 'horsCatalogue'))
        decheck(find_elt_by('ID', d, 'choixCompteSansDossiers'))
        decheck(find_elt_by('ID', d, 'dapBlanche'))
        decheck(find_elt_by('ID', d, 'dapBlancheVoeuUn'))
        decheck(find_elt_by('ID', d, 'dapBlancheVoeuDeux'))
        decheck(find_elt_by('ID', d, 'dapBlancheVoeuTrois'))
        decheck(find_elt_by('ID', d, 'dapJaune'))
        decheck(find_elt_by('ID', d, 'dapJauneVoeuUn'))
        decheck(find_elt_by('ID', d, 'dapJauneVoeuDeux'))
        decheck(find_elt_by('ID', d, 'polytech'))
        decheck(find_elt_by('ID', d, 'iut'))
        decheck(find_elt_by('ID', d, 'autre'))
        decheck(find_elt_by('ID', d, 'fle'))
        decheck(find_elt_by('ID', d, 'cbDateEtatDossier'))  
        decheck(find_elt_by('ID', d, 'cbDatePaiement'))
        decheck(find_elt_by('ID', d, 'cbDateEntretien'))
        check(find_elt_by('ID', d, 'cbMessagesNonTraites'))
        decheck(find_elt_by('ID', d, 'criteres.dispenseTestFrancais'))
        decheck(find_elt_by('ID', d, 'criteres.filtreNiveauFrancais')) 
        decheck(find_elt_by('ID', d, 'criteres.rechercheDifferee'))

        #### boutons radios
        # soumis 1, indifférent 6
        set_radio(6, find_elt_by('ID', d, 'listeEtatsDossier')) 
        set_radio(4, find_elt_by('ID', d, 'listeEtatsPaiement'))
        set_radio(4, find_elt_by('ID', d, 'listeEtatsEntretien'))
        # pour les liste selection (déroulantes) au laisse au defautl
        #### select
        set_option(0, "listeAntennes", d)
        set_option(0, "criteres.acceptPapierAutreCas", d)
        set_option(0, "typeDiplome", d)
        set_option(0, "typeFormationFle", d)
        set_option(0, "criteres.idObjetEtudiant", d)
        set_option(0, "exemptionEntretienPaiement", d)
        set_option(0, "criteres.statutBourse", d)
        set_option(0, "criteres.statutAutre", d)
        set_option(0, "criteres.niveauFrancaisMin", d)
        set_option(0, "criteres.niveauFrancaisMax", d)


def lance_recherche_directe(d):
    # themis lance la recherche
    find_elt_by('ID', d, 'submitButtonRechercherParCriteres').click()

def lance_recherche_differee(d, bname=None):
    """si basename is None, utiliser gedci_export_ y ajoute un time stamp lance la recherche différée et renvois le nom du fichier qui sera téléchargé """
    # Thèmis coche la case 'Recherche différée'
    check(find_elt_by('ID', d, 'criteres.rechercheDifferee'))

    # Thèmis crée le nom du fichier
    # Thèmis récupère la date du moment et la sauvegarde dans une chaine
    if bname is not None:
        now = pd.Timestamp.now().round('60s').strftime("%Y%m%d_%H%M")
        bname = f'{bname}_'+now
        
        # Thèmis renseigne le champs Nom du fichier
        # utilise l'heure de paris par défaut. UTC +1 pour nommer le fichier
        input = find_elt_by('ID', d, ('criteres.nomFichierRechercheDifferee'))
        input.clear()
        input.send_keys(bname)

    # Thèmis envois notre requête
    find_elt_by('ID', d, 'submitButtonRechercherParCriteres').click()
    
    if bname is None:
        return None
    else:
        return bname+'.xls'
    

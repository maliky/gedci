from django.contrib.auth.models import AbstractUser
from django.db import IntegrityError
from django.db import models
import datetime as dt
import functools
import gedci.gedcilib.settings as S
import logging
import pandas as pd

# voir doc à partir de 400, 407
class UserGEDCI(AbstractUser):

    def __str__(self, wuname=False, withGroup=False):
        rep = f"{self.first_name} {self.last_name}"
        rep += f"{self.username}" if wuname else ""
        if withGroup:
            rep += "Groups: "
            for g in self.groups.all():
                rep += f"{g}, "
        return rep
    pass

  
# ################ Importation ################
class Importation(models.Model):

    dateCrea = models.DateTimeField(auto_now_add=True)
    precedente = models.ForeignKey('self', on_delete=models.CASCADE,
                                   related_name='prec', null=True)

    nom_fichier_import = models.CharField(max_length=50, blank=True, null=True)
    date_fichier_import =  models.DateTimeField(blank=True, null=True)

    nb_dossiers = models.PositiveIntegerField(null=True)
    nb_nouveaux_docs = models.PositiveIntegerField(null=True)
    nb_updated_docs = models.PositiveIntegerField(null=True)

    nb_paiements_valides = models.PositiveIntegerField(null=True)
    nb_visa_renseignes = models.PositiveIntegerField(null=True)
    nb_scac_instruit = models.PositiveIntegerField(null=True)
    nb_favorables = models.PositiveIntegerField(null=True)
    nb_defavorables = models.PositiveIntegerField(null=True)
    nb_entretenus2 = models.PositiveIntegerField(null=True)
    nb_soumissions = models.PositiveIntegerField(null=True)
    nb_validations = models.PositiveIntegerField(null=True)
    nb_entretiens_autorises = models.PositiveIntegerField(null=True)
    nb_entretiens_valides = models.PositiveIntegerField(null=True)
    nb_entretiens_demandes = models.PositiveIntegerField(null=True)
    nb_entretiens_planifie = models.PositiveIntegerField(null=True)
    nb_entretiens_annules = models.PositiveIntegerField(null=True)

    def __str__(self, short=True):
        datecrea = strftime(self.dateCrea).split('/')[-1]
        rep = f"Creation {datecrea}, fichier {self.nom_fichier_import}\n"
        if not short:
            stats_dicts = {k:v for (k,v) in self.__dict__.items() if 'nb_' in k }
            rep += f"{stats_dicts}"

        return rep


    def update_stats(self, nb_nouveaux_docs=None, nb_updated_docs=None):
        """update the importation stats"""

        data = {}
        data["nb_dossiers"] = len(Dossier.objects.all())

        if nb_nouveaux_docs is None:
            data["nb_nouveaux_docs"] = len(Dossier.objects.filter(importCrea__pk=self.pk))

        # à vérifier
        if nb_updated_docs is None:
            data["nb_updated_docs"] = len(Dossier.objects.filter(importUpdate__pk=self.pk))

        data["nb_paiements_valides"] = self.getnb_paiements_valides()
        data["nb_visa_renseignes"] = self.getnb_visa_renseignes()
        data["nb_scac_instruit"] = self.getnb_scac_instruit()
        data["nb_favorables"] = self.getnb_favorables()
        data["nb_defavorables"] = self.getnb_defavorables()
        data["nb_entretenus2"] = self.getnb_entretenus2()
        data["nb_soumissions"] = self.getnb_soumissions()
        data["nb_validations"] = self.getnb_validations()
        data["nb_entretiens_autorises"] = self.getnb_entretiens_autorises()
        data["nb_entretiens_valides"] = self.getnb_entretiens_valides()
        data["nb_entretiens_demandes"] = self.getnb_entretiens_demandes()
        data["nb_entretiens_planifie"] = self.getnb_entretiens_planifie()
        data["nb_entretiens_annules"] = self.getnb_entretiens_annules()

        self.__dict__.update(data)
        self.save()

    # methodes génériques
    # vais compter les doublons...un dossiers avec plusieurs états identique
    # mais c'est bon quand même

    def getnb_etatD(self, mot_clef):
        etat_cherche = EtatD.objects.get(nom=mot_clef)
        return len(EtatDossier.objects.filter(etatD=etat_cherche))

    def getnb_etatRDV(self, mot_clef):
        etat_cherche = EtatRDV.objects.get(nom=mot_clef)
        return len(EtatDossier.objects.filter(etatR=etat_cherche))

    def getnb_etatPaiement(self, mot_clef):
        etat_cherche = EtatPaiement.objects.get(nom=mot_clef)
        return len(EtatDossier.objects.filter(etatP=etat_cherche))

    def getnb_paiements_valides(self):
        return self.getnb_etatPaiement(EtatPaiement.VALIDE)

    def getnb_visa_renseignes(self):
        return self.getnb_etatD(EtatD.VISA)

    def getnb_scac_instruit(self):
        return self.getnb_etatD(EtatD.INSTRUIT)

    def getnb_favorables(self):
        return self.getnb_etatD(EtatD.FAVORABLE)

    def getnb_defavorables(self):
        return self.getnb_etatD(EtatD.DEFAVORABLE)

    def getnb_entretenus2(self):
        return self.getnb_etatD(EtatD.ENTRETENU)

    def getnb_soumissions(self):
        return self.getnb_etatD(EtatD.SOUMIS)

    def getnb_validations(self):
        return self.getnb_etatD(EtatD.VALIDE)

    def getnb_entretiens_autorises(self):
        return self.getnb_etatRDV(EtatRDV.AUTORISE)

    def getnb_entretiens_valides(self):
        return self.getnb_etatRDV(EtatRDV.VALIDE)

    def getnb_entretiens_demandes(self):
        return self.getnb_etatRDV(EtatRDV.DEMANDE)

    def getnb_entretiens_planifie(self):
        return self.getnb_etatRDV(EtatRDV.PLANIFIE)

    def getnb_entretiens_annules(self):
        return self.getnb_etatRDV(EtatRDV.DEMANDEDANNULATION)

# ################ LES ETATS ################

@functools.total_ordering
class AbstractEtat(models.Model):
    NONCREE = S.NONCREE
    VALIDE = S.VALIDE
    INDIFFERENT = S.INDIFFERENT
    NONAUTORISE = S.NONAUTORISE

    TYPES = {NONCREE: 0,
             VALIDE: 5,
             INDIFFERENT: 0,
             NONAUTORISE: 0}

    CHOIX = [(k, k) for (k, v) in TYPES.items()]

    nom = models.CharField(unique=True,
                           choices=CHOIX,
                           max_length=50,
                           null=True)

    num = models.PositiveSmallIntegerField()  

    def __str__(self):
        return f'{self.nom}'

    def _is_valid_operand(self, other):
        return self.__class__ == other.__class__

    def __lt__(self, other):
        if not self._is_valid_operand(other):
            cname = m.self.__class__
            raise Exception("Ne peux comparer que deux {cname}")
        else:
            return ((self.num < other.num) and
                    (self.nom < other.nom))

    class Meta:
        abstract = True


class EtatAtt(AbstractEtat):
    FAITE = S.FAITE
    ENCOURS = S.ENCOURS

    TYPES = {ENCOURS: 1,
            FAITE: 2,
            **AbstractEtat.TYPES}
    # eventuellement voir un truc du genre CHOIX = lambda ...
    CHOIX = [(k, k) for (k, v) in TYPES.items()]


class EtatD(AbstractEtat):
    DEFAVORABLE = S.DEFAVORABLE
    ENSAISIE = S.ENSAISIE
    ENTRETENU = S.ENTRETENU
    FAVORABLE = S.FAVORABLE
    INSTRUIT = S.INSTRUIT
    SOUMIS = S.SOUMIS
    VISA = S.VISA

    TYPES = {DEFAVORABLE: 5,
             ENSAISIE: 1,
             ENTRETENU: 3,
             FAVORABLE: 5,
             INSTRUIT: 6,
             SOUMIS: 2,
             VISA: 7,
             **AbstractEtat.TYPES}

    CHOIX = [(k, k) for (k, v) in TYPES.items()]
class EtatPaiement(AbstractEtat):

    AUTORISE = S.AUTORISE
    DECLARE = S.DECLARE
    DEMANDE = S.DEMANDE
    PASDEMANDE = S.PASDEMANDE
    PLANIFIE = S.PLANIFIE

    TYPES = {AUTORISE: 3,
             DECLARE: 4,
             DEMANDE: 1,
             PASDEMANDE: 0,
             PLANIFIE: 1,
             **AbstractEtat.TYPES}

    CHOIX = [(k, k) for (k, v) in TYPES.items()]

class EtatRDV(AbstractEtat):

    AUTORISE = S.AUTORISE
    DEMANDE = S.DEMANDE
    DEMANDEDANNULATION = S.DEMANDEDANNULATION
    PASDEMANDE = S.PASDEMANDE
    PLANIFIE = S.PLANIFIE

    TYPES = {AUTORISE: 3,
             DEMANDE: 1,
             DEMANDEDANNULATION: 4,
             PASDEMANDE: 0,
             PLANIFIE: 2,
             **AbstractEtat.TYPES}

# ################ Etat dossier ################
@functools.total_ordering
class EtatDossier(models.Model):

    dateChg = models.DateTimeField(auto_now_add=True)  # check doc p. 1099

    dateFSoum = models.DateTimeField(blank=True, null=True)  # date first soumission
    dateLSoum = models.DateTimeField(blank=True, null=True)  # date last soumission
    dateEnt = models.DateTimeField(blank=True, null=True)  # date entretien
    dateVDos = models.DateTimeField(blank=True, null=True)  # date validation
    datePai = models.DateTimeField(blank=True, null=True)  # date validation paiem

    etatD = models.ForeignKey('EtatD', on_delete=models.CASCADE,
                              related_name='etatD', null=True)
    etatP = models.ForeignKey('EtatPaiement', on_delete=models.CASCADE,
                              related_name='etatP', null=True)
    etatR = models.ForeignKey('EtatRDV', on_delete=models.CASCADE,
                              related_name='etatR', null=True)
    precedent = models.ForeignKey('self', on_delete=models.CASCADE,
                                  related_name='prec', null=True)

    def __str__(self, prec=False):
        def filtre(s):
            """filtre le string s"""
            return s in ['dateChg', 'dateFSoum', 'dateLSoum', 'dateEnt', 'dateVDos', 'datePai']

        dates = {k:strftime(v) for (k,v) in self.__dict__.items() if filtre(k)}

        rep = "Dernière soumission le %s:\n" % dates['dateLSoum']
        rep += f"Dossier: {self.etatD} "
        rep += f"({dates['dateVDos']}), " if self.dateVDos is not None else ", "
        rep += f"Paiement: {self.etatP} "
        rep += f"({dates['datePai']}), " if self.datePai is not None else ", "
        rep += f"RDV: {self.etatR} "
        rep += f"({dates['dateEnt']}), " if self.dateEnt is not None else ", "

        if prec:
            rep += '>>Etat précédent<<: ' + self.precedent.__str__()
        return rep

    def _is_valid_operand(self, other):
        return self.__class__ == other.__class__


    def __eq__(self, other):
        """est égale si les noms des états sont égaux et si la date est proche à 10 minutes"""
        if not self._is_valid_operand(other):
            cname = self.__class__
            cname2 = other.__class__
            raise Exception(f"Ne peux pas comparer (=)  {cname} et {cname2}")
        else:
            return ((self.etatD.nom == other.etatD.nom) and
                    (self.etatP.nom == other.etatP.nom) and
                    (self.etatR.nom == other.etatR.nom) and
                    (self.dateFSoum == other.dateFSoum) and
                    (self.dateLSoum == other.dateLSoum))

    def __lt__(self, other):
        if not self._is_valid_operand(other):
            cname = m.self.__class__
            raise Exception(f"Ne peux comparer (<) que deux {cname}")
        else:
            # il faudrait bien étudier cette règle
            return ((self.nb_etats(S.SOUMIS) < other.nb_etats(S.SOUMIS)) or
                    (self.nb_etats(S.ENSAISIE) < other.nb_etats(S.ENSAISIE)) or
                    (self.etatD < other.etatD) or
                    ((self.etatD == other.etatD) and
                     (self.etatP < other.etatP)) or
                    ((self.etatD == other.etatD) and
                     (self.etatP == other.etatP) and
                     (self.etatR < other.etatR)) or
                    ((self.etatD == other.etatD) and
                     (self.etatP == other.etatP) and
                     (self.etatP == other.etatP) and
                     (rounddt(self.dateChg,'600s') < rounddt(other.dateChg,'600s'))))

        
    def nb_etats(self, etatd_nom=None):
        if self.precedent is None:
            return 0
        elif etatd_nom is None:
            return 1 + self.precedent.nb_etats()
        else:
            if self.etatD.nom == etatd_nom:
                return 1 + self.precedent.nb_etats(etatd_nom)
            else:
                return self.precedent.nb_etats(etatd_nom)

        

# ################ Models ################
class Dossier(models.Model):

    dateCrea = models.DateTimeField(auto_now_add=True)
    dateLastModif = models.DateTimeField(auto_now=True)
    # par défaut null=False
    numDos = models.CharField(max_length=20)  # no need to set as primary_key
    typeDos = models.CharField(max_length=50, blank=True, null=True)
    prenomEtu = models.CharField(max_length=50, blank=True, null=True)  # blank pour les dossiers juste crées et vide
    nomEtu = models.CharField(max_length=50, blank=True, null=True)
    emailEtu = models.EmailField(blank=True, null=True)
    dateNaisEtu = models.DateField(blank=True, null=True)
    etabO = models.CharField(max_length=200, blank=True, null=True)  # Etab. origine
    etabV = models.CharField(max_length=200, blank=True, null=True)
    nivoAtteint = models.CharField(max_length=20, blank=True, null=True)
    nivoVise = models.CharField(max_length=20, blank=True, null=True)
    urlDos = models.URLField(blank=True, null=True)

    etatsDoc = models.ManyToManyField(EtatDossier)  # etat Inter Dossier

    nbSoum = models.PositiveSmallIntegerField(default=0)

    stats = models.ForeignKey('DocStat', on_delete=models.CASCADE,
                              related_name='stats', null=True)

    importCrea = models.ForeignKey('Importation', on_delete=models.CASCADE,
                                   related_name='importdeCrea', null=True)
    importUpdate = models.ManyToManyField(Importation)

    def __str__(self, size='short'):
        """size can be short, medium ou long"""
        rep = f'{self.numDos}:{self.nomEtu}'
        if size != "short":
            rep +=f"({self.dateNaisEtu})\n"
            rep +=f"De: {self.etabO} ({self.nivoAtteint})\n"
            rep +=f"A: {self.etabV} ({self.nivoVise})\n"
        if size == "long":
            rep += f"{self.etatsDoc.last()}"
        return rep

    def _is_valid_operand(self, other):
        return self.__class__ == other.__class__

    def __lt__(self, other):
        if not self._is_valid_operand(other):
            cname = m.self.__class__
            raise Exception("Ne peux comparer que deux {cname}")
        else:
            # il faudrait bien étudier cette règle
            return ((self.etatsDoc < other.etatsDoc) or
                    self.getnb_soumissions() < other.getnb_soumissions())

        # Les dossier sont classé dans l'ordre suivants
        # 1 "En saisie étudiant"
        # 2 "Soumis"
        # 3 "Synthèse d'entretien terminée"
        # 5 "Défavorable"
        # 5 "Favorable"
        # 5 "Instruction SCAC terminée"
        # 7 "Décision Visa Renseignée"

        # S'il y a des égalité on regarde 
        # le nombre de fois que le dossier a été soumis


    def get_etat_recent(self):
        """renvois l'état le plus récent, None s'il n'y en a pas"""
        return self.etatsDoc.order_by('-dateChg').first()

    def dates_changed_since(self, date):
        ldates = ["dateLSoum", "dateVDos", "dateEnt", "datePai"]
        # récupère les valeurs des dates enregistrées
        ldates_val = [self.__dict__[d] for d in ldates]
        # on les compare à notre date
        evall= [date < d for d in ldates_val if d != None]

        # si l'une d'elle est plus grande (donc récente que la date fournie)
        # c'est qu'il y a eu changement)
        return any(evall)

    def changed_since_(self, date):
        """return true if one of the date last soumission, validation paiement is older than date"""
        ldates = ["dateLSoum", "dateVDos", "dateEnt", "datePai"]
        ldates_val = [self.__dict__[d] for d in ldates]
        evall= [date < d for d in ldates_val if d != None]
        # logging.info("%s %s" % (self, evall))
        return any(evall)

    def update_dossier(self, **data):
        """data est un dict avec les var de dossier et leur valeurs. ['typeDos', 'prenomEtu', 'nomEtu', 'emailEtu', 'dateNaisEtu', 'etabO', 'etabV', 'nivoAtteint', 'nivoVise', 'etatDDist', 'etatPDist', 'etatRDist', 'dateFSoum', 'dateLSoum', 'dateVDos', 'dateEnt', 'datePai']\n
        elles sont toutes optionnelles"""
        korData = {k: data[v] for (k,v) in S.KORCOLSD.items()}
        etatData = {k: data[v] for (k,v) in S.ETATCOLSD.items()}

        self.update_kerdoc(**korData)
        self.update_Etatdossier(**etatData)


    # ################ STAT SUR LES ÉTATS DU DOSSIER ################
    def getnb_changements_etats(self):
        # il s'agit en fait du nombre de soumissions mais je n'ai pas accès à la distinction du type de soumission
        return len(self.etatsDoc.all())

    def getnb_etatD(self, mot_clef):
        etat_cherche = EtatD.objects.get(nom=mot_clef)
        return len(self.etatsDoc.filter(etatD=etat_cherche))

    def getnb_etatRDV(self, mot_clef):
        etat_cherche = EtatRDV.objects.get(nom=mot_clef)
        logging.info(f"{self} and {etat_cherche}")
        return len(self.etatsDoc.filter(etatR=etat_cherche))

    def getnb_etatPaiement(self, mot_clef):
        etat_cherche = EtatPaiement.objects.get(nom=mot_clef)
        return len(self.etatsDoc.filter(etatP=etat_cherche))

    def getnb_soumissions(self):
        return self.getnb_etatD(EtatD.SOUMIS)

    def getnb_validations(self):
        return self.getnb_etatD(EtatD.VALIDE)

    def getnb_entretiens_autorises(self):
        return self.getnb_etatRDV(EtatRDV.AUTORISE)

    def getnb_entretiens_demandes(self):
        return self.getnb_etatRDV(EtatRDV.DEMANDE)

    def getnb_entretiens_planifie(self):
        return self.getnb_etatRDV(EtatRDV.PLANIFIE)

    def getnb_entretiens_annules(self):
        return self.getnb_etatRDV(EtatRDV.DEMANDEDANNULATION)

    # ################ UPDATE ################

    def update_kerdoc(self, **data):
        """given optional data dict de la forme (var:value) à mettre à jour met à jour.
Il s'agit normalement des options du ker (du corps, du coeur, du noyaux) du dossier qui ne change pas aussi souvent que les options d'état"""
        self.__dict__.update(data)

    def update_Etatdossier(self, update_stat=False, **opts):
        "les options devraient contenir les var etatD, etatP, etatR, dateFSoum, dateLSoum, dateVDos, dateEnt, dateEnt, datePai"
        # get the last etatsDoc if exist, otherwise .first returns none
        oldEd = self.etatsDoc.all().order_by('-dateChg').first()
        # s'écrit aussi oldEd = self.etatsDoc.all().latest('dateChg')

        tetatsD = {'etatD': EtatD, 'etatP': EtatPaiement, 'etatR': EtatRDV}
        for (k,v) in tetatsD.items():
            opts[k] = get_etat(v, nom=opts[k])

        newED = EtatDossier(precedent=oldEd, **opts)
        for (k,v) in opts.items():
            if opts[k] == 'NaT':
                opts[k] = ''
        try:
            if (oldEd is not None and oldEd != newED) or (oldEd is None):
                newED.save()
                self.etatsDoc.add(newED)
                if update_stat:
                    self._update_DocStat()

        except Exception as e:
            raise
        

        return newED


    def _update_DocStat(self):
        """Ce fait après  la mise à jour des états dossier"""

        data = {}
        data["nb_etats"] = self.getnb_changements_etats()
        data["nb_soumissions"] = self.getnb_soumissions()
        data["nb_validations"] = self.getnb_validations()
        data["nb_entretiens_autorises"] = self.getnb_entretiens_autorises()
        data["nb_entretiens_demandes"] = self.getnb_entretiens_demandes()
        data["nb_entretiens_planifie"] = self.getnb_entretiens_planifie()
        data["nb_entretiens_annules"] = self.getnb_entretiens_annules()

        try:
            docstat = self.stats
            if docstat is None:
                # on doit le créer
                docstat = DocStat()
                self.stats = docstat

            docstat.update(**data)
            docstat.save()
        except Exception as e:
            logging.warning("Il faut probablement en créer une nouvelle Stat")
            raise e

        self.save()


@functools.total_ordering
class Attribution(models.Model):
    CORRECTION = S.CORRECTION
    ENTRETIEN = S.ENTRETIEN
    
    CHOIX_ACTIONS = {CORRECTION: CORRECTION,
                     ENTRETIEN: ENTRETIEN}

    dateCrea = models.DateTimeField(auto_now_add=True)
    dateModif = models.DateTimeField(auto_now=True)

    auteur = models.ForeignKey(UserGEDCI, related_name='aut_attrib',
                               on_delete=models.CASCADE)

    acteur = models.ForeignKey(UserGEDCI, related_name='act_attrib',
                               on_delete=models.CASCADE)

    action = models.CharField(max_length=20,
                              choices=list(CHOIX_ACTIONS.items()))

    # the related name is for the reveser naming.  dossier.attributions
    dossier = models.ForeignKey(Dossier, on_delete=models.CASCADE,
                                related_name='attributions', null=True)

    # état de l'attribution
    etatAtt = models.ForeignKey('EtatAtt', on_delete=models.CASCADE,
                                related_name='tions', null=True) 

    def _is_valid_operand(self, other):
        return isinstance(other, Attribution)

    def __lt__(self, other):
        if (self is not None) and (other is 0):
            return False
        elif (self is None):
            return True
        elif not self._is_valid_operand(other):
            raise Exception("Ne peux comparer que deux Attribution enter elles")
        else:
            return ((self.auteur == other.auteur) and
                    (self.acteur == other.acteur) and
                    (self.action == other.action) and
                    (self.dossier == other.dossier) and
                    (self.etatAtt == other.etatAtt))

    def __str__(self, full=False):
        dateModif = self.dateCrea.strftime("%d-%b %H:%M")
        rep = f'{self.action} par {self.acteur}: {self.etatAtt} ({dateModif})'

        if full:
            rep += f'\n{docset}'
        return rep



def get_etat(Letat, nom=None):
    """Renvois l'état de nom nom si existe"""
    try:
        etat = Letat.objects.get(nom=nom)
    except Letat.DoesNotExist as dne:
        logging.warning("Attention l'%s demandé '%s' n'est pas connu. Renvoi etat de None" % (Letat, nom))
        etat = Letat.objects.get(nom=None)
        logging.warning(dne)

    return etat



class DocStat(models.Model):
    # models.DecimalField(max_digits=5, decimal_places=2, null=True)
    dateCrea = models.DateTimeField(auto_now_add=True)
    dateUpdate = models.DateTimeField(auto_now=True)

    # precedente = models.ForeignKey('self', on_delete=models.CASCADE,
    #                                related_name='prec', null=True)
    nb_etats = models.PositiveIntegerField(null=True)
    nb_soumissions = models.PositiveIntegerField(null=True)
    nb_validations = models.PositiveIntegerField(null=True)
    nb_entretiens_autorises = models.PositiveIntegerField(null=True)
    nb_entretiens_demandes = models.PositiveIntegerField(null=True)
    nb_entretiens_planifie = models.PositiveIntegerField(null=True)
    nb_entretiens_annules = models.PositiveIntegerField(null=True)

    def update(self, **data):
        """update the stat data"""
        self.__dict__.update(data)

    def __str__(self):
        datecrea = strftime(self.dateCrea)
        dateupdate = strftime(self.dateUpdate)
        rep = f"Creation {datecrea}, Update {dateupdate}\n"
        stats = {k:v for (k,v) in self.__dict__.items()  if 'nb_' in k}
        rep += f"Stats: {stats}"
        return f"{rep}"


def strftime(v):
    if v is not None:
        if not isinstance(v, str):
            return v.strftime("%Y-%m-%d %H:%M:%S")
        else:
            return v

def rounddt(datetime, unit='s'):
    return dt.datetime.fromtimestamp(pd.Timestamp(datetime).round(unit).timestamp())
    

# Generated by Django 2.0.3 on 2018-11-24 09:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gedci', '0025_auto_20181123_1628'),
    ]

    operations = [
        migrations.RenameField(
            model_name='dossier',
            old_name='etatDos',
            new_name='etatsDoc',
        ),
    ]

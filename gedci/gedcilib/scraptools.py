from selenium import webdriver
from .mlksoup import get_linked_table, beauty
from .navigate import page_fichiers_generes
import time
import logging


def init_wd_firefox(time, dwldir='./', headless=True, proxy=None):
    print("Initialisation Driver Firefox")
    dPf = webdriver.FirefoxProfile()
    dPf.set_preference('browser.download.dir', dwldir)
    dPf.set_preference('browser.download.folderList', 2)
    dPf.set_preference("browser.helperApps.neverAsk.saveToDisk",
                       "text/csv,application/vnd.ms-excel")

    d = webdriver.Firefox(firefox_profile=dPf, firefox_options=None)

    d.implicitly_wait(time)
    print("Implicite waiting time set to %d" % time)
    return d


def find_elt_by(func, d, text):
    """Soit un type func parmis LINK, CSS ou ID, un driver et un text correspondant à func.  Appel la fonction selenium correspondant à func.
    C'est juste un raccourci et harmonisation de notation"""
    if func == 'LINK':
        elt = d.find_element_by_link_text(text)
    elif func == 'CSS':
        elt = d.find_element_by_css_selector(text)
    elif func == 'ID':
        elt = d.find_element_by_id(text)
    return elt


def find_elts_css(d, text):
    return d.find_elements_by_css_selector(text)


def telecharge_fichier_genere_malik(d, stable=None, lktab=None, ieme=0):
    """Soit un web driver d, une soupe table stable  (par défaut le premier tableau de la page en cours)\n
    et un ieme (defaut 0).\n
    télécharge le ième fichier généré par KONE Malik\n
    Bien penser à avoir avoir une nouvelle table après la suppression
    déplace le fichier téléchargé dans dstdir, par défaut ./
    renvoi lktab le tableau de téléchargement avec les liens"""
    
    # on récupère un df de la stable
    if lktab is None and stable is not None:
        lktab = get_linked_table(stable, alinks=[5])
    elif lktab is None and stable is None:
        raise Exception("lktab et stable ne peuvent pas être null tous deux en même temps")

    # on récupère le css de l'élément à cliquer pour supprimer le ieme elt
    csselt = lktab.where(lktab.demandePar == 'KONE Malik').dropna().action.iloc[ieme]['Télécharger']
    # on clique sur l'élément
    find_elt_by('CSS', d, csselt).click()
    # que faire en cas de demande de confirmation?

    return lktab


def supprime_fichier_genere_malik(d, csselt):
    """Soit un web driver d, une get_linked_table lktab et un ieme (defaut None)\
    supprime le ième fichier généré par KONE Malik\n
    si ieme = * supprime tout.
    Bien penser à avoir avoir une nouvelle table après la suppression"""
    # on récupère le css de l'élément à cliquer pour supprimer le ieme elt
    click_supprime_fichier_genere(d, csselt)
    time.sleep(5)
    page_fichiers_generes(d)
    time.sleep(5)
    stable = beauty(d).table


def supprimer_fichiers_generes_malik(d, stable=None, lktab=None, ieme='*'):
    """given a driver de on the page of fichiers générés.  supprime les fichiers générés par malik, les liens supprimé si succès, False sinon"""
    # on récupère un df de la stable
    if lktab is None and stable is not None:
        lktab = get_linked_table(stable, alinks=[5])
    elif lktab is None and stable is None:
        raise Exception("lktab et stable ne peuvent pas être null tous deux en même temps")

    liensASup = get_actions_malik(lktab=lktab, filtre="Supprimer")

    if ieme == '*':
        while len(liensASup):
            csselt = liensASup[0]
            click_supprime_fichier_genere(d, csselt)
            time.sleep(5)
            page_fichiers_generes(d)
            time.sleep(5)
            stable = beauty(d).table
            time.sleep(4)
            liensASup = get_actions_malik(stable=stable, filtre="Supprimer")
            time.sleep(1)
    elif ieme is None:
        return True
    else:
        try:
            csselt = liensASup[ieme]
            click_supprime_fichier_genere(d, csselt)
        except IndexError as ie:
            logging.warning("Probably trying to remove non existing lines %s" % ie.__str__())
            pass

    return False


def get_actions_malik(stable=None, lktab=None, filtre=''):
    """Retourne la liste de lien dans la 5ème colonne du tableau"""
    if lktab is None and stable is not None:
        lktab = get_linked_table(stable, alinks=[5])
    elif lktab is None and stable is None:
        raise Exception("lktab et stable ne peuvent pas être null tous deux en même temps")

    actions = lktab.where(lktab.demandePar == 'KONE Malik').action.dropna()

    liensASup = []
    for action in actions:
        if filtre in action.keys():
            liensASup.append(action[filtre])

    return liensASup



def click_supprime_fichier_genere(d, csselt):
    """supprimes le fichiers en lot.  le site ne recharge pas la page automatiquement donc ok"""
    # execture la suppression en séquence
    find_elt_by('CSS', d, csselt).click()
    time.sleep(2)
    try:
        # prompt for confirmation
        # n'apparaît pas toujours
        print(d)
        elt = find_elt_by('CSS', d, 'div.jsoverlay-footer button')
        time.sleep(2)
        elt.click()
    except Exception as e:
        logging.info(e)
        pass


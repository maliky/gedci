from django.contrib.auth.models import Group
import re
import logging
from gedci import models as m
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from gedci.gedcilib import settings as S


def get_Dos_context(order, filtre):
    """Return a dic with all contex but major part is the list of docs sorted and filter by parameters"""
    nom_filtre = S.FILTRE_ETATD[filtre] if filtre != 'tous' else 'tous'
    context = {"actions": m.Attribution().CHOIX_ACTIONS.keys(),
               "dataset": get_docs(order, filtre),
               "order": order,
               "filtre": filtre,
               "nom_filtre": nom_filtre}
    # ajout des élement utile pour le menu des cdp
    context.update(cdp_menu_context())
    return context


def cdp_menu_context():
    """Renvois un dict des élément nécessaire pour le menu des cdp"""
    group_acteurs = get_groups_acteurs()
    
    return {"groups_acteurs": get_groups_acteurs(),
            "acteurs": get_acteurs(),
            "last_import": m.Importation.objects.latest('dateCrea'),
            "filtres": S.FILTRES}


def get_docs(order, filtre=None):
    """order est l'une 'id', 'typeDos',  'numDos', 'nomEtu' ,'prenomEtu', 'dateNaisEtu'
    Retourne data qui est un dic {"id": d.id, "typeDos": d.typeDos, "numDos": d.numDos[:-4], "nomEtu": d.nomEtu, "prenomEtu": d.prenomEtu, "dateNaisEtu": d.dateNaisEtu, "etat": ed, "attrib":att}"""

    # le filtre
    docs = set()
    if filtre in S.FILTRE_ETATD:
        nom_etat = S.FILTRE_ETATD[filtre]
        etds = m.EtatDossier.objects.filter(etatD__nom=nom_etat)
        for etd in etds:
            docs |= set(etd.dossier_set.all())
    elif filtre == "tous":
        docs = m.Dossier.objects.all()

    list_etats = [d.etatsDoc.last() for d in docs]

    # #### etat ####
    data = []  # une liste de dictionnaire
    for (doc, etat) in zip(map(to_dict, docs), list_etats):
        doc['etat'] = etat
        data.append(doc)

    # #### ATTRIBUTION ####
    # on récupère la dernière attribution des dossiers
    for doc in data:
        attrib = None
        try:
            attrib = (m.Attribution.objects.filter(dossier__id=doc['id'])
                      .latest('dateModif'))
        except IndexError as ie:
            print(f">>>>{ie}<<<<")
            logging.warning(ie)
            logging.warning(f"acces dossier {id} non existant on dirait")
            pass
        except m.Attribution.DoesNotExist as dne:
            attrib = None
        doc['attrib'] = attrib


    # #### TRI ####
    # on trie les dossier selon la clef order
    # gérer le cas order = etat et attrib
    order = S.SORT_ORDERS[order]

    if order.startswith("-"):
        reverse = True
        order = order[1:]
    else:
        reverse = False

    data = sorted(list(data), key=lambda doc: sort_function(doc, order),
                  reverse=reverse)
    return data


def sort_function(doc, order='numDos'):
    """récupère un dossier et doit le classer selon différents critères
    {typeDos numDos nomEtu etat attrib"""
    if order in ["typeDos", "numDos", "nomEtu", "attrib", "etat"]:
        if doc[order] is None:
            # logging.warning(f"PRINT THIS DOC -->>{doc}<<-- et key:{order}")
            # raise Exception("None type")
            if order == "attrib":
                return 0

        return doc[order]
    if order == "etatAtt":
        return doc['attrib'].etatAtt


def get_docAtt(user, order, filtre=None):
    """retoure la liste des dossiers attribués à un user.
    order est une clef de tri des dossiers retournés"""
    # on récupère les dossiers et pour chaque dernier étatD
    list_dossiers, list_etats = [], []
    # d'abors toutes les attributions de l'acteur filtré si necessaire

    attL = m.Attribution.objects.filter(acteur=user)

    if filtre in S.FILTRE_ATTRIB and (filtre is not None):
        attL = attL.filter(etatAtt__nom=S.FILTRE_ATTRIB[filtre])

    # pour chaque attribution garde le dossier son dernier état et l'attribution
    docAttL = []
    for att in attL:
        # il faut s'assurer to_dict ne renvois pas déjà les clefs att et etat
        docAtt = to_dict(att.dossier)
        docAtt['attrib'] = att
        docAtt['etat'] = att.dossier.etatsDoc.last()
        docAttL.append(docAtt)

    order = S.SORT_ORDERS[order]

    if order.startswith("-"):
        reverse = True
        order = order[1:]
    else:
        reverse = False

        #    print(docAttL)
    docAtt = sorted(docAttL, key=lambda da: sort_function(da, order),
                    reverse=reverse)
    return docAtt


def get_checkbox_id(request, pat=None):
    """Renvois un liste de macth de rpat and cherché dans les valeurs du dict de request.POST
    Utile pour récupérer une valeur dans le nom d'un checkbox par exemple"""

    pat = "checkbox-(.*)" if pat is None else "checkbox-"+pat
    rpat = re.compile(pat)

    return [v for (k, v) in request.POST.items() if rpat.search(k) is not None]


def get_Attrib_actions():
    "user the user making the request, gnames une liste de groups names"
    return list(m.Attribution().CHOIX_ACTIONS.keys())


def get_acteurs(groups=["correcteurs", "interviewers"]):
    """get une liste de membre des groups"""
    users = m.UserGEDCI.objects.all()
    return set(users.filter(groups__name__in=groups))


def get_groups_acteurs():
    """Retourne l'ensemble des correcteurs ou interviewers"""
    gps = Group.objects.filter(name__in=['correcteurs', 'interviewers'])
    groups = {}
    for g in gps:
        groups[g] = list(g.user_set.all())
    return list(groups.items())


def set_pagination(request, dataset, default_page=1, pager=None, max_page=25):
    """given a dataset avec (an iteration of stuff to display) et une page renvois le context avec la pagination"""
    page = rget(request, "page", default=default_page)

    if pager is None:
        pager = Paginator(dataset, max_page)

    try:
        dataset = pager.get_page(page)
    except EmptyPage as ep:
        dataset = []
        logging.error(ep)
        pass
    except PageNotAnInteger as pnai:
        dataset = []
        logging.error(pnai)
        pass

    return dataset


def get_pages(request):
    return [v for (k, v) in request.GET.items() if k == 'page']


def to_dict(d):
    """Filtre un dossier en dictionnaire
    Ou récupère les donnes qui nous intersses dans un dictionnaire"""
    return {"id": d.id,
            "typeDos": d.typeDos,
            "numDos": d.numDos[:-4],
            "nomEtu": d.nomEtu,
            "prenomEtu": d.prenomEtu,
            "dateNaisEtu": d.dateNaisEtu,
            "chgEtats": d.getnb_changements_etats(),
            "nbSoum": d.getnb_soumissions(),
            "nbEnt": d.getnb_entretiens_autorises(),
            "etatbO": d.etabO,
            "etabV": d.etabV,
            "nivoAtteint": d.nivoAtteint}


def update_attributions(nouveauEtat, attIds, attUser):
    """Met à jour les attribution des docs_pk, action doit être un des """
    print(f"{nouveauEtat} et ettypvalues %s" % m.EtatAtt.TYPES.keys())
    if nouveauEtat not in m.EtatAtt.TYPES.keys():
        raise Exception(f"action doit être dans %s" % m.EtatAtt.TYPES.keys())

    # pour chaque attribution désigné modifie l'état avec nouveau Etat
    for att in m.Attribution.objects.filter(pk__in=attIds):
        try:
            newET = m.EtatAtt.objects.get(nom=nouveauEtat)
        except m.EtatAtt.DoesNotExist as dne:
            logging.warning("{dne} Donc création population de la table m.EtatAtt")
            newET = m.EtatAtt(nom=nouveauEtat)
            newET.save()

        att.etatAtt = newET
        att.save()

    return get_attributions(attUser)


def get_attributions(user):
    """Returns a list of all user's attributions as acteur"""
    attributions = m.Attribution.objects.filter(acteur=user).order_by("-dateModif")

    return attributions


def get_attrib_context(attUser, attributions):
    """Returns some context for the Attribution View function"""
    etatChoix = set(m.EtatAtt.CHOIX) - set(m.AbstractEtat.CHOIX)

    return {"actions": list(etatChoix),
            "attUser": attUser,
            "attributions": attributions}


def est_dans_group(user, gname="chef"):
    """return la liste des group chef donc vrai, """
    return [g for g in user.groups.all() if gname in g.name]


def rget(request, key, default):

    if request.method == 'POST':
        return rpost(request, key, default)
    # sinon le get
    try:
        var = request.GET[key]
    except KeyError as ke:
        if default is None:
            logging.warning(f"Pas de default pour request.GET[{key}]")
            raise ke
        else:
            var = default
    return var


def rpost(request, key, default):
    try:
        var = request.POST[key]
    except KeyError as ke:
        if default is None:
            logging.warning(f"Pas de default pour request.POST[{key}]")
            raise ke
        else:
            var = default
    return var


def get_model_instance(model, pk):
    """does a simple get(pk=pk) on the model instances"""
    try:
        rep = model.objects.get(pk=pk)
    except Exception as e:
        logging.warning(f"Problem getting key {pk} of model {model}")
        raise e
    return rep


def set_attributions(request, acteurid, docIds):
    """an actor id, a list of doc ids"""
    try:
        etatt = m.EtatAtt.objects.get(nom=m.EtatAtt.ENCOURS)
    except m.EtatAtt.DoesNotExist as dne:
        logging.warning(f"{dne}, Création d'un nouvel EtatAtt {EtatAtt.ENCOURS}")
        etatt = EtatAtt(nom=m.EtatAtt.ENCOURS)
        etatt.save()

    attL = []
    for d in m.Dossier.objects.filter(id__in=docIds):
        attribution = m.Attribution(auteur=request.user,
                                    acteur=m.UserGEDCI.objects.get(id=acteurid),
                                    action=request.POST['action'],
                                    etatAtt=etatt,
                                    dossier=d)
        attribution.save()
        attL.append(attribution)

    return attL


def maj_attributions(acteur, docIds, etatMaj):
    """an actor id, a list of doc ids"""
    try:
        etatatt = m.EtatAtt.objects.get(nom=etatMaj)
    except m.EtatAtt.DoesNotExist as dne:
        logging.warning(f"{dne}, Création d'un nouvel EtatAtt {etatMaj}")
        etatatt = m.EtatAtt(nom=etatMaj, num=m.EtatAtt.TYPES[etatMaj])
        etatatt.save()

    # récupère l'ancienne attribution qui a cette état pour ce user
    updated_attribL = []
    for d in m.Dossier.objects.filter(id__in=docIds):
        old_attrib = d.attributions.filter(acteur=acteur).latest('dateModif')
        old_attrib.etatAtt = etatatt
        old_attrib.save()
        updated_attribL.append(old_attrib)

    return updated_attribL


def get_etatsdoc_context(doc, etatsdocs):
    """Returns some context for the DocEtasView"""
    return {"dataset": etatsdocs,
            "doc": doc,
            "doc_stats": doc.stats,
            "order": "doc"}

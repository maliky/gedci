from django.core.management.base import BaseCommand, CommandError
from gedci.gedcilib.func4models import reload_data

import logging


class Command(BaseCommand):
    help = "Choisir le dossier où chercher le fichier csv et son nom"

    def add_arguments(self, parser):
        parser.add_argument('-d', '--srcdir', type=str, default='./Data',
                            help="dossier de téléchargement (default ./Data)")
        parser.add_argument('-b', '--bname', type=str,
                            help='nom de base au quel sera ajouté l\'extension appropriée, (default gedci_export_)')
        parser.add_argument('-l', '--log_level', type=str, default='INFO',
                            help='set the log level (default INFO)')

    def handle(self, *args, **options):
        logging.getLogger("").setLevel(options["log_level"])

        bname = options['bname']
        try:
            reload_data(bname=options["bname"],
                        srcdir=options["srcdir"])
        except Exception as e:
            raise CommandError("check %s" % e)

        self.stdout.write(self.style.SUCCESS('Successfully Done "%s"' % bname))

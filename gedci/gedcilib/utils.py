import pandas as pd
import os
import re
import datetime as dt
import logging

def is_file(fname):
    """renvois true or false depending if file pas condition"""
    #  isabs isdir isfile islink ismount
    return os.path.isfile(fname)

def fcond(f, pat):
    """set a condition on a file name"""
    rpat = re.compile(pat)
    return rpat.search(f)


def is_xls_file(f):
    return fcond(f, pat=r'\.xls$')


def is_csv_file(f):
    return fcond(f, pat=r'\.csv$')


def sort_param(fname):
    """renvois un nombre qui permet de classer le fichier fname"""
    #  getatime getctime *getmtime* getsize
    return os.path.getmtime(fname)


def get_latest_export_basename(srcdir=None):
    """returns the latest export file name.  If the srcdir is not given returns the cwd"""
    "KONE_Malik_export_dossiers_11_09_2018_00_31.xls"
    fname = get_cond_recent_file(rang=0, srcdir=srcdir, coln='fname', fcond=is_file)
    bname = ''.join(fname.split('.')[:-1])
    return bname


def get_recent_file(rang=0, srcdir=None):
    """soit un rang 0 à nb fichiers dans srcdir, renvois la date du fichier à ce rang class du plus récent au plus ancien.
   srcdir (=cwd by default)"""
    return get_cond_recent_file(rang=rang, srcdir=srcdir, coln='fname', fcond=is_file)


def get_recent_xls_file(rang=0, srcdir=None):
    return get_cond_recent_file(rang=rang, srcdir=srcdir, coln='fname', fcond=is_xls_file)


def get_most_recent_csv_file(srcdir=None):
    return get_cond_recent_file(srcdir=srcdir, rang=0, coln='fname', fcond=is_csv_file)


def get_recent_date(rang=0, srcdir=None):
    """soit un rang 0 à nb fichiers dans srcdir, renvois la date du fichier à ce rang class du plus récent au plus ancien.
   srcdir (=cwd by default)"""
    return get_cond_recent_file(srcdir=srcdir, rang=rang, coln='getmtime', fcond=is_xls_file)


def get_cond_recent_file(rang=0, srcdir=None, coln='fname', fcond=lambda x: os.path.isfile(x)):
    """soit un rang 0 à nb fichiers dans srcdir, renvois la date du fichier à ce rang class du plus récent au plus ancien si pat (def \.xls$) en regex est dans sont fullname (avec path)
    srcdir (=cwd by default)"""
    return get_cond_recent_info(rang=rang, srcdir=srcdir, coln=coln, fcond=fcond)


def get_cond_recent_info(rang=0, srcdir=None, coln='fname', fcond=lambda x: os.path.isfile(x)):
    """soit un rang 0 à nb fichiers dans srcdir, renvois la date du fichier à ce rang class du plus récent au plus ancien si pat (def \.xls$) en regex est dans sont fullname (avec path)
    srcdir (=cwd by default)"""
    if srcdir is None:
        srcdir = os.getcwd()

    # get list of all files in dir
    fullnames = ['/'.join([srcdir, f]) for f in os.listdir(srcdir)]

    # keep file (type f) and adjoinct a getmtime value
    fnames = [(f, os.path.getmtime(f)) for f in fullnames if fcond(f)]
    # logging.info(fnames)
    # put this in a dataFrame
    colfilen = 'fname'
    coldaten = 'getmtime'
    df = pd.DataFrame(fnames, columns=[colfilen, coldaten])

    # parse date col to_datetime format
    df.loc[:, coldaten] = pd.to_datetime(df.loc[:, coldaten],
                                         unit='s')
    # sort
    df = df.sort_values(coldaten)[::-1]

    res = None
    
    try:
        res = df.iloc[rang].loc[coln]
    except IndexError as ie:
        logging.warning(f"srcdir={srcdir} et fcond={fcond}")
        logging.warning(f"rang={rang} et coln={coln} dans df(head)\n%s" % df.head(3))
        raise

    return res


def get_extraction_date(ffname, offset=dt.timedelta(seconds=3600)):
    """Parse the ffname which should be an extraction file to get the date from it and return the date - offset (default 1 hour)"""
    # extracting the date from the name
    pat = r'_(?P<day>\d+)_(?P<month>\d+)_(?P<year>\d+)_(?P<hour>\d+)_(?P<minute>\d+)\..{3}$'
    # logging.info(f'pat={pat}\n{ffname}')
    trouvailles = re.compile(pat).search(ffname).groupdict()

    # convertir en int et crée la date
    # to get date previous extraction (qui si généré par le système doit être en phase avec le contenu de la df)
    dpextract = dt.datetime(**{k: int(v) for (k, v) in trouvailles.items()})

    # On lui enlève une heure créer un chevauchement et ne pas rater de mise à jour
    # sachant qu'il y a peu de nouveaux dossiers dans l'heure qui suit les extractions
    return dpextract - offset


def get_previous_extraction_date(srcdir='./Data'):
    """get the second to the most recent extraction file name from dir srcdir, 
    parse the name to get the date from it and return the date"""
    # before most recent file name
    bmrfn = get_recent_xls_file(1, srcdir)
    return get_extraction_date(bmrfn)


def une_date_est_plus_jeune(serieDeDates, dateref):
    """compare les dates d'une série avec une date de référence et renvois vrai si l'une des dates de la série est plus jeunes (plus grande) que la date de référence"""
    return any([dateref < d for d in serieDeDates])


def can_be_date(dt):
    return not (dt in ['', None] or pd.isna(dt))


def datetime_conversion(df, datetimecols):
    for coln in datetimecols:
        df.loc[:, coln] = df.loc[:, coln].apply(datetime_conv)
    return df


def simpledate_conversion(df, puredatecols):
    for coln in puredatecols:
        df.loc[:, coln] = df.loc[:, coln].apply(date_conv)
    return df


# #### Date conversion ####
def date_conv(dn):
    """Soit dn une date au format string, vérifie des erreurs et renvois la date au format %Y-%d-%m"""
    if can_be_date(dn):
        try:
            pat = r'(?P<year>\d+)(?:-|/)(?P<month>\d+)(?:/|-)(?P<day>\d+)'
            trouvailles = re.compile(pat).search(dn).groups()
            day, month, year = map(int, trouvailles)
            year = 1900 if (1930 > year) or (year > (pd.Timestamp.now().year - 10)) else year
            month = 1 if month > 12 else month
            daysinmonth = pd.Timestamp(year=year, month=month, day=1).daysinmonth
            day = 1 if day > daysinmonth else day
            try:
                ts = pd.Timestamp(year=year, month=month, day=day).strftime("%Y-%m-%d")
                return ts
            except OutOfBoundsDatetime as obd:
                logging.error(f"pb avec {dn} dans {obd}")
                raise obd
        except Exception as e:
            logging.warning(f"Check {dn}")
            raise(e)
        
    return None


def datetime_conv(dt):
    """Soit une Datetime dt la converti en Y-m-d"""
    if can_be_date(dt):
        try:
            ts = pd.Timestamp(dt).round('ms')
            return ts.strftime("%Y-%m-%d %H:%M")
        except ValueError as ve:
            logging.warning("dt of type %s = '%s'" % (type(dt), dt))
            raise
    return None


from django.core.management.base import BaseCommand, CommandError
import gedci.gedcilib.utils as u
import gedci.models as m
import gedci.gedcilib.settings as S
import gedci.gedcilib.func4models as f
import pandas as pd
import logging


class GEDCILoader():
    def __init__(self, df=None, bname=None, srcdir=None, date_coerce=True, date_filter=True, erase=False):
        """Class qui charge un fichier (bname) ou un df et lance la mise à jour de la DB avec les fichiers qui sont nouveaux ou ont changé depuis la dernière extraction
        argument peuvent être df, date_coerce, bname, srcdir"""
        self.erase = erase
        if erase:
            logging.info("Repopulating")
            f.repopulate_db()
            
        self.date_coerce = date_coerce
        self.datecols = [v for (k, v) in S.ETATCOLSD.items() if 'date'in k.lower()]
        self.srcdir = './Data' if srcdir is None else srcdir
        self.date_filter = date_filter

        if df is None:
            if bname is not None:
                self.ffname = f"{self.srcdir}/{bname}.csv"
                logging.info(f"{bname} et {self.ffname}")
            else:
                self.ffname = u.get_most_recent_csv_file(self.srcdir)
                logging.info(f"Fetching {self.ffname}")

            if self.date_coerce:
                logging.info(f'Coercing dates')
                self.df = pd.read_csv(f'{self.ffname}', index_col=0, header=0, parse_dates=self.datecols)
            else:
                self.df = pd.read_csv(f'{self.ffname}', index_col=0, header=0)
        else:
            self.df = df

        # voir s'il faut travail les colones de dates
        # on s'assure que l'index contien les numéro de dossier
        self.df = self.df.set_index(keys=["N° de dossier"])

        if self.date_filter:

            # fait un mask de filtrage
            # on ne garde les indices que des row qui satisfont la condition
            # (au moins une date plus jeune)
            logging.info("Filtering entries")
            mask = self.df.loc[:, self.datecols].apply(self.date_test, axis=1).values
        else:
            mask = [True] * len(self.df)
            logging.info("Skipping date filtering")

        # restore les type str de la df
        def datetime_tostr(serie):
            s = serie.apply(str)
            for idx in s.index:
                if s[idx] == "NaT":
                    s[idx] = None
            return s
    
        # reconvertir les dates en string pour faciliter l'injection dans django
        logging.info("checking dates")
        self.df.loc[:, self.datecols] = (self.df.loc[:, self.datecols]
                                         .apply(datetime_tostr, axis=1))

        # On collecte les docs qui ont une date au moins plus jeune que ...
        self.changedDocs = self.df.loc[mask, :]
        logging.info("Initialisation GEDCILoader done !")

    def date_test(self, serie):
        """renvois vraie si il y a au moins un élément de la liste de booléens indicantsi l'entrée dans la série est plus jeune que la date précédente d'extraction est vrai"""
        prev_extraction_date = u.get_previous_extraction_date(self.srcdir)
        return u.une_date_est_plus_jeune(serie, prev_extraction_date)

    def run(self):
        # df = df.replace(to_replace=numpy.datetime64('NaT'), value=None)

        # #### effectue la mise à jour ou création des nouveau dossier
        # en parcourrant les dossiers à changer un par un
        updatedDocs, newDocs = [], []
        updatedDocsID, newDocsID = [], []
        i, j = 0, 0

        logging.info("Entering the data loader the loop")

        for idx in self.changedDocs.index.values:
            data = self.changedDocs.loc[idx, :]

            try:
                d = m.Dossier.objects.get(numDos=idx)
                # traitement à faire
                d.update_dossier(**data.to_dict())
                
                d.save()
                updatedDocsID.append(idx)
                updatedDocs.append(d)
                i += 1
                # logging.info(f"{d} updated {i}")
                pass
            except m.Dossier.DoesNotExist as dne:

                d = f.update_or_create_doc(idx, **data)
                newDocsID.append(idx)
                newDocs.append(d)
                d.save()
                # logging.info(f"{d} created {j}")
                j += 1
                pass
            print(f"updatedDocs {i}, newDocs {j}, count {i+j}/%s" % len(self.changedDocs),
                  end="\r")
        print(f"updatedDocs {i}, newDocs {j}, count {i+j}/%s" % len(self.changedDocs))

        self.save_importation(newDocs=newDocs, updatedDocs=updatedDocs)

    def save_importation(self, newDocs, updatedDocs):
        # ajout de l'importation dans la base de données
        fname = self.ffname.split('/')[-1]
        extract_date = u.get_extraction_date(self.ffname)

        try:
            prec = m.Importation.objects.filter(dateCrea__isnull=False).latest('dateCrea')
        except m.Importation.DoesNotExist as dne:
            logging.warning("Ne devrait passer ici que lors de l'initialisation")
            prec = None
            
        new_import = m.Importation(nom_fichier_import=self.ffname,
                                   date_fichier_import=extract_date,
                                   precedente=prec)

        new_import.save()

        for d in newDocs:
            d.importCrea = new_import
            d.save()

        for d in updatedDocs:
            d.importUpdate.add(new_import)
            d.save()

        new_import.update_stats()

class Command(BaseCommand):
    help = "charge un fichier csv dans la db."

    def add_arguments(self, parser):
        parser.add_argument('-d', '--srcdir', type=str, default='./Data',
                            help="Source dire (defaut ./Data)")
        parser.add_argument('-b', '--bname', type=str,
                            help="nom du fichier a enregistrer sans l'extension csv. Par defaut plus récent csv de scrdir")
        parser.add_argument('-c', '--date_coerce', action="store_false",
                            help="if NOT present does a date conversion")
        parser.add_argument('-f', '--date_filter', action="store_false",
                            help="if NOT present does a date filtering")
        parser.add_argument('-l', '--log_level', type=str, default='INFO',
                            help='set the log level (default INFO)')
        parser.add_argument('-x', '--erase', action="store_true",
                            help='If presente erase all data in the database and repopulate')

    def handle(self, *args, **options):

        bname = options['bname']
        srcdir = options['srcdir']
        date_coerce = options['date_coerce']
        date_filter = options['date_filter']
        erase = options['erase']
        if erase:
            logging.info(f"Setting date_filter to {date_filter} car option -x is (erase)")
            date_filter = False
        logging.getLogger("").setLevel(options["log_level"])

        gedciLoader = GEDCILoader(bname=bname,
                                  srcdir=srcdir,
                                  date_coerce=date_coerce,
                                  date_filter=date_filter,
                                  erase=erase)

        try:
            gedciLoader.run()
            logging.info("GEDCILoader run finished !")
        except CommandError as e:
            gedciScrapper.d.close()
            raise CommandError("%s, au moment du loading" % e)

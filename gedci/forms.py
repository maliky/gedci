from django.forms import ModelForm
from gedci.models import Dossier, Importation


class ImportationForm(ModelForm):
    class Meta:
        model = Importation
        fields = '__all__'

class DossierForm(ModelForm):
    class Meta:
        model = Dossier
        fields = '__all__'

        # class LoginForm(forms.Form):
#     login_name = forms.CharField(label='Login Name', max_length=100)
#     mot_de_passe = forms.CharField(label='Mot de passe', max_length=100)
    


from django.core.management.base import BaseCommand, CommandError
import gedci.gedcilib.func4models as f

import logging


class Command(BaseCommand):
    help = "Choisir le dossier où chercher le fichier csv et son nom"

    def add_arguments(self, parser):
        parser.add_argument('-d', '--srcdir', type=str, default='./Data',
                            help="dossier où se trouve le fichier à charger ( ./Data)")
        parser.add_argument('-b', '--bname', type=str,
                            help='nom de base au quel sera ajouté l\'extension appropriée, (default le plus récent)')
        parser.add_argument('-l', '--log_level', type=str, default='INFO',
                            help='set the log level (default INFO)')

    def handle(self, *args, **options):
        logging.getLogger("").setLevel(options["log_level"])

        bname = options["bname"]

        self.stdout.write(self.style.SUCCESS('RE-Loading "%s"' % bname))
        try:
            f.repopulate_db()
            f.reload_data(bname=bname,
                          srcdir=options["srcdir"])
        except Exception as e:
            raise CommandError("check %s" % e)

        self.stdout.write(self.style.SUCCESS('Successfully Done "%s"' % bname))
        

from django.db import IntegrityError
from gedci.models import Dossier, EtatDossier, EtatD, EtatPaiement, EtatRDV
from gedci.models import Importation, EtatAtt, Attribution, DocStat
from gedci.models import UserGEDCI as UG
from django.contrib.auth.models import Group
import gedci.gedcilib.settings as S
import gedci.gedcilib.utils as u
import logging
import pandas as pd
import numpy as np
import datetime as dt


def add_init_EtatDossier(listDocs, ed):
    "list docs et un état doss"
    logging.info(f"updating docs with {ed}")
    i = 0
    for d in listDocs:
        i += 1
        print("%s, %s/%s" % (d, i, len(listDocs)), end='\r')
        d.etatsDoc.add(ed)
        d.save()


def update_EtatDossiers(listDocs):
    """update l'état du dossier avec son état récupéré par la df"""
    i = 0
    for d in listDocs:
        i += 1
        try:
            # d.save()
            d.update_etat_dossier()
        except Exception as e:
            logging.error(e)
            raise

        print("%s/%s    " % (i, len(listDocs)), end='\r')
        logging.info("Update des EtatDossier Done !")


def erase_all_db():
    """Delete everything"""
    erase_etats()
    db_tables = [Importation, EtatDossier, EtatAtt, Attribution, Dossier, DocStat]
    for C in db_tables:
        C.objects.all().delete()
    pass


def erase_etats():
    lesEtats = [EtatD, EtatPaiement, EtatRDV, EtatAtt]
    
    for C in [le for le in lesEtats if len(le.objects.all())]:
        C.objects.all().delete()

    return lesEtats


def repopulate_db():
    erase_all_db()
    repopulate_lesetats()
    # repopulate_default_users()
    pass

def repopulate_default_users():
    create_default_users()
    create_default_groups()
    set_default_memberships()
    logging.info(f"Default UserGEDCI reloaded")
    pass

def populate_etat(Letat):
    """Rempli la table avec des valeurs autorisées
    Attention s'il y a des  dossiers attaché il faudra les rechargés"""
    Letat.objects.all().delete()
    for (k, v) in Letat.TYPES.items():
        le = Letat()  # ou type(Letat).__call__(Letat)
        le.nom = k
        le.num = v
        try:
            le.save()
        except IntegrityError as ie:
            logging.warning("%s.  Il y aura probablement des duplicates" % ie)
            pass

        
def repopulate_lesetats():
    """Populate les trois classe d'états """
    lesEtats = erase_etats()
    for C in lesEtats:
        populate_etat(C)
    logging.info(f'Population des classes {lesEtats} Done!')
    pass    


def reload_data(bname=None, srcdir='./Data'):
    """Charge un fichier csv venant de gedci_wrangling dans la base de donnée.
    bname: non de base du fichier à charger sans extension 'defaut the latest modified? in the srcdir (2ème arg) qui par défaut est './Data'"""
    # Initialisation des variables de noms
    if bname in [None, '']:
        ffname = "."+u.get_latest_export_basename(srcdir)+'.csv'
    else:
        ffname = f"{srcdir}/{bname}.csv"

    # On charge tout le fichier exporté
    df = pd.read_csv(f'{ffname}', index_col=0, header=0)
    df = df.set_index(keys=["N° de dossier"])

    # On charge les dossiers dans la BD
    load_data(df)


def load_data(df):
    """Charge les données de la table df and la db, l'index doit être le num de dossier et la df doit avoir été nétoyé au niveau des NAT"""

    i = 0
    for idx in df.index:
        data = df.loc[idx].to_dict()

        update_or_create_doc(idx, **data)
        i += 1
        print("Dossier %s/%s màj ou crée." % (i, len(df)), end="\r")

    print("Load data Done!")


def update_or_create_doc(numDos, importation=None,  **data):
    """créer un nouveau dossier  partir d'une ligne (idx) de données
data est un dict avec le nom des colonnes en clef et leur valeurs,
la vérification de l'unicité du num de dossier doit avoir été faite avant et des dates"""

    def naToNone(elt):
        """On s'assure que les données pour la db sont OK ni '' ni nan"""
        # logging.info(elt)
        try:
            if pd.isna(elt) or (elt in ['', 'NaT', 'nan']):
                return None
        except TypeError as e:
            # elt est probablement un str ou une date
            logging.warning(e)
            pass
        return elt

    numDos = numDos
    # on récupère la valeur du data à l'aide des dic de trad col -> var

    kerOptionsD = {k: naToNone(data[v]) for (k, v) in S.KORCOLSD.items()}

    # we try to update the doc if it does not existe, we create it
    # importation?, stats ?
    try:
        d = Dossier.objects.get(numDos=numDos)
        d.update_kerdoc(**kerOptionsD)
    except Dossier.DoesNotExist as dne:
        d = Dossier(numDos=numDos, **kerOptionsD)
        # logging.info(f"{d}, {kerOptionsD}")
        try:
            d.save()
        except django.core.exceptions.ValidationError as ve:
            print(d.__str__(size="long"))


    optEtatD = {k: naToNone(data[v]) for (k, v) in S.ETATCOLSD.items()}
    # print(optEtatD)
    # raise
    d.update_Etatdossier(**optEtatD)

    return d

def create_default_users():
    # voir tmp/users.py
    users = []
    users.append({'username': 'jrose', 'email': 'jrose@gedci.app', 'password':  'LeSilence',
                  'first_name':  'Joséphine', 'last_name':  'Rose'})
    users.append({'username': 'mmoutarde', 'email': 'mmoutarde@gedci.app', 'password':  'LeSilence',
                  'first_name':  'Michael', 'last_name':  'Moutarde'})
    users.append({'username': 'bleblanc', 'email': 'bleblanc@gedci.app', 'password':  'LeSilence',
                  'first_name':  'Blanche', 'last_name':  'Leblanc'})
    users.append({'username': 'jolive', 'email': 'jolive@gedci.app', 'password':  'LeSilence',
                  'first_name':  'John', 'last_name':  'Olive'})
    users.append({'username': 'ppervenche', 'email': 'ppervenche@gedci.app', 'password':  'LeSilence',
                  'first_name':  'Patricia', 'last_name':  'Pervenche'})
    users.append({'username': 'pviolet', 'email': 'pviolet@gedci.app', 'password':  'LeSilence',
                  'first_name':  'Peter', 'last_name':  'Violet'})
    for u in users:
        user = UG.objects.create_user(**u)
        user.save()


def create_default_groups():
    groups = ["chefs_de_poles", "correcteurs", "interviewers"]
    for g in groups:
        group = Group(name=g)
        group.save()


def set_default_memberships():
    groups = {'jrose': ['chefs_de_poles'],
              'mmoutarde': ['chefs_de_poles'],
              'bleblanc': ['interviewers', 'chefs_de_poles'],
              'jolive': ['interviewers'],
              'ppervenche': ['interviewers', 'correcteurs'],
              'pviolet': ['correcteurs']}

    for (k, gps) in groups.items():
        u = UG.objects.get(username=k)
        gps = Group.objects.filter(name__in=gps)
        for g in gps:
            u.groups.add(g)
            u.save()

    # pour l'administration.
    staff_user = Group.objects.get(name="chefs_de_poles").user_set.all()
    for su in staff_user:
        su.is_staff = True
        su.save()

    jr = UG.objects.get(username='jrose')
    jr.is_superuser = True
    jr.save()

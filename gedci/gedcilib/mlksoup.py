from bs4 import BeautifulSoup
import pandas as pd


def beauty(d):
    """soit d un web driver.  Renvois le code de la page en cours"""
    return BeautifulSoup(d.page_source, 'html.parser')


def get_cellule_links(td):
    """Soit un élément td du DOM.\n
    Extrait les liens contenu dans la cellule et renvois sous forme de dict un css sector permettant de les récupérer avec un webdriver"""
    links = td.find_all('a')
    # on parcours chaque ancre
    linkD = {}
    for a in links:
        href = a.get('href')
        td_ = a.findPrevious('td')
        if href in ["#", ""]:
            # si l'ancre n'a pas de href, on regard l'action de la form précédent
            tagcss = 'td#' + td_.get('id') + ' a.' + '.'.join(a.get('class'))
            linkD[a.text] = tagcss
        else:
            # sinon c'est la href simplement
            tagcss = 'td#' + td_.get('id') + ' a'
            linkD[a.text] = tagcss

    return linkD


def get_entete_table(stable):
    """Soit une soup.table stable, renvois une list avec le nom des entetes corectement formaté"""
    return [th.get('id') for th in stable.find_all('th')]


def get_linked_table(stable, alinks=[5]):
    """Soit une soup.table stable renvois un df avec cette table ou les (des) links des colones (alinks) récupérés dans un dictionnaires"""
    body = list()
    # on parcours le body    
    for tr in stable.find_all('tr'):
        trl = list()
        # pour chaque ligne on analyse les cellules td
        trs = tr.find_all('td')
        for j in range(len(trs)):
            td = trs[j]
            if j in alinks:
                # si on est dans une colonne alinks
                trl.append(get_cellule_links(td))
            else:
                # sinon on renvois simplement le text
                trl.append(list(td.stripped_strings)[0])

        body.append(trl)
    df = pd.DataFrame(body)[1:]
    df.columns = get_entete_table(stable)
    return df

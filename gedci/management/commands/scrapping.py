from django.core.management.base import BaseCommand, CommandError
from gedci.gedcilib import forms, navigate, scraptools, mlksoup, arguments, utils
from time import sleep
import logging
import shutil


class GEDCIScrapper():

    def __init__(self, dwldir, bname=None, dstdir=None, display=False):
        """Class qui va sur le site de campus france, génère un fichie d'export et le sauvegarde sur le disque"""

        self.dwldir = dwldir
        self.dstdir = "./Data" if dstdir in [None, '', False] else dstdir
        self.bname = bname
        try:
            if display:
                from pyvirtualdisplay import Display
                display = Display(visible=0, size=(1024, 900), use_xauth=True)
                display.start()
                logging.info('running with virtual display')

            self.d = scraptools.init_wd_firefox(2, dwldir=dwldir, headless=False, proxy=False)
            logging.info('Init of driver done')
        except Exception:
            raise

    def run_download(self):

        logging.info('Logging to cF ?')
        navigate.logging_CF(self.d)
        pause(30, "Done.  Waiting %s")

        logging.info('Navigate à la page de recherche ?')
        navigate.page_recherche(self.d)
        pause(20, "Done.  Waiting %s")

        logging.info('Remplissage du formulaire ?')
        forms.remplir_formulaire_recherche(self.d, default=True)
        pause(10, "Done.  Waiting %s")

        logging.info('lancement recherche differee ?')
        fname = forms.lance_recherche_differee(self.d, bname=self.bname)
        pause(120, "Done.  Waiting %ss")

        logging.info('Aller à la page des fichiers générés ?')
        navigate.page_fichiers_generes(self.d)
        pause(20, "Done.  Waiting %ss")

        logging.info("Patiente jusqu'à la fin du de la génération")
        lktab = navigate.attend_fin_generation(self.d,
                                               stable=mlksoup.beauty(self.d).table)

        logging.info('Téléchargement du fichier généré ?')
        lktab = scraptools.telecharge_fichier_genere_malik(self.d,
                                                           lktab=lktab,
                                                           ieme=0)
        logging.info(f'Done. Fichier {fname} enregistré dans rep de téléchargement du navigateur (normalement {self.dwldir})')
        sleep(1)

        try:
            rfname = utils.get_recent_file(srcdir=self.dwldir)
            # move or copy it
            shutil.copy(src=rfname, dst=self.dstdir)
        except shutil.SameFileError as sfe:
            logging.info(f"Attention with file {rfname} is already in {self.dstdir}\n Qu'en est-il de {self.dwldir}/{fname}?")
            pass
        logging.info(f'Copy du fichier {rfname} de {self.dwldir} -> {self.dstdir}')

        logging.info('Suppression des fichiers générés ?')
        scraptools.supprimer_fichiers_generes_malik(self.d, lktab=lktab, ieme="*")
        pause(1, "Done.  Waiting %ss")

        logging.info('driver closed ?')
        self.d.close()
        logging.info('Done.')


def pause(delay=10, message="Waiting %s"):
    logging.info(message % delay)
    sleep(delay)


class Command(BaseCommand):
    help = "Choisir le bname, le dwldir et s'il faut démarrer avec ou sans écran virtuel"

    def add_arguments(self, parser):
        parser.add_argument('-w', '--dwldir', type=str, default='~/Download',
                            help="dossier de téléchargement (default ./Data)")
        parser.add_argument('-d', '--dstdir', type=str, default='./Data',
                            help="dossier de destination du fichier (default ./Data)")
        parser.add_argument('-b', '--bname', type=str,
                            help="nom du fichier a enregistrer sans l'extension xls. si None défaut du site. mieux pour gère des multi-site.")
        # check verbose
        parser.add_argument('-l', '--log_level', type=str, default='INFO',
                            help='set the log level (default INFO)')
        parser.add_argument('-s', '--virtual_screen', action="store_true",
                            help="si présent utilise un écran virtuel sinon non")

    def handle(self, *args, **options):
        # dépend de la plateforme.  soit /home/mlk/Download/ soit /home/mlk/Téléchargement ou TI..
        dwldir = options["dwldir"]
        dstdir = options["dstdir"] 
        bname = options["bname"]  # ne plus utiliser
        virtual = options["virtual_screen"]
        logging.getLogger("").setLevel(options["log_level"])

        self.stdout.write(self.style.SUCCESS(f'Scraping with virtual-screen={virtual}'))
        gedciScrapper = GEDCIScrapper(dwldir, dstdir, bname, display=virtual)

        try:
            gedciScrapper.run_download()
        except Exception as e:
            gedciScrapper.d.close()
            raise CommandError("%s, au moment du scrapping" % e)

from django.shortcuts import render,
redirect
from django.contrib.auth.models import Group
from gedci.models import UserGEDCI, Attribution, Dossier
from django.views import View

# ################ ./gedci/view.py ################


class Accueil(View):
    def get(self, request):
    # J'arrive ici que si mon user est identifié
        print("GET=%s" % request.GET)
        return redirect('/gedci/login/')
    
    def post(self, request):
        print("POST=%s" % request.POST)
        # J'arrive ici que si mon user est identifié
        print("request.user? %s" % request.user)
        print("request.user.groups? %s" % request.user.groups.all())
        if len(request.user.groups.filter(name__in=['chefs', 'admin'])):
            return redirect('/gedci/cdp')

        elif len(request.user.groups.filter(name__in=['correcteurs', 'interviewers'])):
            return redirect('/gedci/conseillers')


# ################ gedci/urls.py ################

from django.urls import path
from django.contrib.auth import views as auth_views
from django.contrib.auth.views import LoginView, LogoutView
from . import views as gedci_views

app_name = 'gedci'

urlpatterns = [
    path('accueil', gedci_views.Accueil.as_view(), name='accueil'),
    path('conseillers/', gedci_views.Conseillers.as_view(), name='conseillers'),
    path('cdp/', gedci_views.Chefs2Pole.as_view(), name='cdp'),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
]

# ################ IFCI/urls.py ################

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.views.generic import RedirectView

urlpatterns = [
    path('gedci/', include('gedci.urls')),
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),

   # les redirects
    path('accounts/login/', RedirectView.as_view(url='/gedci/login/')),
    path('accounts/profile/', RedirectView.as_view(url='/gedci/accueil/')),
    path('accounts/logout/', RedirectView.as_view(url='/gedci/logout')), 
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


# ################ IFCI/settings.py ################
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = '-z05mw8=%i(-mpr8)x2l4yajx-kiklzfzf1%67uajd&(rj34uj'

DEBUG = True

ALLOWED_HOSTS = ['gedci-dev.institufrancais.support', 'localhost']

# Application definition

INSTALLED_APPS = [
    'gedci.apps.GedciConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'IFCI.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'IFCI.wsgi.application'


# Database

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Password validation

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization

LANGUAGE_CODE = 'fr-fr'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)

STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static/'),
]

# Redirection
# ici ou non ?
LOGIN_REDIRECT_URL = 'gedci/'  # peut-être pas au bon endroit

AUTH_USER_MODEL = 'gedci.UserGEDCI'


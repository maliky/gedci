from django.core.management.base import BaseCommand, CommandError
from gedci.gedcilib import utils as u
from pandas.errors import OutOfBoundsDatetime
import logging
import numpy as np
import pandas as pd


class GEDCIWrangler():

    def __init__(self, bname=None, srcdir='./Data'):
        if bname is None or bname == '':
            self.ffname = '.' + u.get_latest_export_basename(srcdir)
        else:
            self.ffname = f"{srcdir}/{bname}"

        self.df = pd.DataFrame()
        
        self.colnames = ['N° de dossier', 'Type de dossier', 'Prénom', 'Nom', 'Adresse mail', 'Date de naissance', 'Dernier établissement du dossier académique', 'Nom groupement – établissement choisi', 'Niveau d’études atteint', 'Niveau d’étude visé', 'Etat du dossier', "Etat de l’entretien", 'Etat de paiement', 'Date de première soumission', 'Date de dernière soumission du dossier', 'Date de validation du dossier', "Date de l’entretien", 'Date de validation du paiement']
        self.datetimecols = ['Date de première soumission', 'Date de dernière soumission du dossier', 'Date de validation du dossier', "Date de l’entretien", 'Date de validation du paiement']

    def load_file(self):
        self.df = pd.read_excel(f"{self.ffname}.xls", header=0, skiprows=3, dtype=str)
        self.df.index.name = "gedci"

    def replace_nans(self):
        self.df = self.df.replace(to_replace='nan', value=np.nan)

    def date_naiss_conversion(self, coln='Date de naissance'):
        self.df = u.simpledate_conversion(self.df, [coln])

    def datetime_conversion(self):
        self.df = u.datetime_conversion(self.df, self.datetimecols)

    def save_to_csv(self):
        self.df.loc[:, self.colnames].to_csv(f"{self.ffname}.csv")
        return self.df

    def run(self):
        self.load_file()
        logging.info(f"{self.ffname}.xls loaded!")

        self.replace_nans()
        logging.info("Replacement des nans done or (should be)!")

        self.date_naiss_conversion()
        logging.info("Convertion date naissance done!")

        self.datetime_conversion()
        logging.info("Convertion des datetime done!")

        self.save_to_csv()
        logging.info(f"Sauvegarde dans {self.ffname}.csv done!")


class Command(BaseCommand):
    help = "récupère le fichier xls téléchargé et le transforme en csv."

    def add_arguments(self, parser):
        parser.add_argument('-d', '--srcdir', type=str, default='./Data',
                            help="Source dire (defaut ./Data)")
        parser.add_argument('-b', '--bname', type=str,
                            help="nom du fichier a enregistrer sans l'extension xls. Par defaut plus récent xls de scrdir")
        parser.add_argument('-l', '--log_level', type=str, default='INFO',
                            help='set the log level (default INFO)')

    def handle(self, *args, **options):
        srcdir = options["srcdir"]
        bname = options["bname"]
        logging.getLogger("").setLevel(options["log_level"])
        
        gedciWrangler = GEDCIWrangler(bname, srcdir)
        try:
            gedciWrangler.run()
        except Exception as e:
            raise CommandError("%s, au moment du Wrangling" % e)

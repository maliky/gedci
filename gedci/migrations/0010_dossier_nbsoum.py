# Generated by Django 2.0.3 on 2018-11-12 11:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gedci', '0009_auto_20181112_1050'),
    ]

    operations = [
        migrations.AddField(
            model_name='dossier',
            name='nbSoum',
            field=models.PositiveSmallIntegerField(default=0),
        ),
    ]

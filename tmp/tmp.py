from django.shortcuts import render

def myView(request):
    n = 10
    data = zip(range(n), sting.ascii_letters[:n])
    return render(request,
                  'myapp/myTemplate.html',
                  context={'data': data}
    )


#### and in myTemplate.html

{% for d in data %}
  {% if d%2 == 0 %}   # d mod 2 
    <p>{{d[0]}}</p>
  {% else %}
    <p>{{d[1]}}</p>
  {% endif %}
{% endfor %}

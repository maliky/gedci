from django.core.management.base import BaseCommand, CommandError
import pandas as pd
from .scrapping import GEDCIScrapper
from .wrangling import GEDCIWrangler
from .loader import GEDCILoader
import logging


class Command(BaseCommand):
    help = "Choisir le dossier où chercher le fichier csv et son nom"

    def add_arguments(self, parser):

        parser.add_argument('-w', '--dwldir', type=str, default='/home/mlk/Downloads/',
                            help="dossier de téléchargement (default ./Data)")
        parser.add_argument('-d', '--dstdir', type=str, default='./Data',
                            help="dossier destination")
        parser.add_argument('-s', '--virtual_screen', action="store_true",
                            help="si présent utilise un écran virtuel sinon non")
        parser.add_argument('-c', '--date_coerce', action="store_false",
                            help="if NOT present does a date conversion")
        parser.add_argument('-l', '--log_level', type=str, default='INFO',
                            help='set the log level (default INFO)')

    def handle(self, *args, **options):

        logging.getLogger("").setLevel(options["log_level"])

        # ################ SCRAPPING ################
                
        #        fname = 'gedci_export_dossier_20185113_1251'

        # récupère le fichier d'export en ligne et le sauvegarde sur le disque
        gedciScrapper = GEDCIScrapper(dwldir=options["dwldir"],
                                      display=options["virtual_screen"])
        try:
            gedciScrapper.run_download()
        except CommandError as e:
            raise Exception("%s, au moment du scrapping" % e)

        # ################ WRANGLING ################
        
        # récupère le fichier sur le disque et le convertis en csv avec le bon format de colonnes
        gedciWrangler = GEDCIWrangler()
        df = pd.DataFrame()
        try:
            df = gedciWrangler.run()
        except CommandError as e:
            raise Exception("%s, au moment du wrangling." % e)

        # ################ LOADER ################
        date_coerce=options['date_coerce']
        gedciLoader = GEDCILoader(date_coerce=date_coerce)

        try:
            gedciLoader.run()
            logging.info("GEDCILoader run finished !")
        except CommandError as e:
            gedciScrapper.d.close()
            raise CommandError("%s, au moment du loading" % e)

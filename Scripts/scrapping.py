import logging
from time import sleep
from gedci.gedcilib import forms, navigate, scraptools, mlksoup, arguments


class GEDCIScrapper():

    def __init__(self, dwldir, basename, display=False):
        self.dwldir = dwldir
        self.basename = basename
        try:
            if display:
                from pyvirtualdisplay import Display
                display = Display(visible=0, size=(1024, 900), use_xauth=True)
                display.start()
                logging.info('running with virtualdispaly')

            self.d = scraptools.init_wd_firefox(2, dwldir=dwldir, headless=False, proxy=False)
            logging.info('Init of driver done')
        except Exception:
            raise
        logging.info('waiting 10s')
        sleep(10)
        
    def run_download(self):
        # initialisation du driver
        # penser à faire une classe
 
        # on se log
        logging.info('Logging to cF ?')
        navigate.logging_CF(self.d, dwldir=self.dwldir)
        logging.info('Done. Waiting 10s')
        sleep(10)

        logging.info('Navigate à la page de recherche ?')
        navigate.page_recherche(self.d)
        logging.info('Done. waiting 10s')
        sleep(10)

        # do_defaut_filling
        logging.info('Remplissage du formulaire ?')
        forms.remplir_formulaire_recherche(self.d, default=True)
        logging.info('Done. waiting 15s')
        sleep(5)

        # lancer la recherche différée
        logging.info('lancement recherche differee ?')
        forms.lance_recherche_differee(self.d, basename=self.basename)
        logging.info('Done. waiting 1min')
        sleep(120)

        logging.info('Aller à la page des fichiers générés ?')
        navigate.page_fichiers_generes(self.d)
        logging.info('Done. wainting 10s')
        sleep(10)

        logging.info("Patiente jusqu'à la 'in du de la génération")
        lktab = navigate.attend_fin_generation(self.d,
                                               stable=mlksoup.beauty(self.d).table)

        logging.info('Téléchargement du fichier généré ?')
        lktab = scraptools.telecharge_fichier_genere_malik(self.d,
                                                           lktab=lktab,
                                                           ieme=0)
        logging.info(f'Done. Fichier {self.basename}.xls enregistré dans rep par défaut du navigateur ou dans {self.dwldir}')
        sleep(10)

        logging.info('Suppression des fichiers générés ?')
        scraptools.supprimer_fichiers_generes_malik(self.d, lktab=lktab, ieme="*")
        logging.info('Done. waiting 10s')
        sleep(10)

        logging.info('driver closed ?')
        self.d.close()
        logging.info('Done.')

# #### Script ####


# voir get arguments from command ligne in kola
args = arguments.get_scrapping_args()

if __name__ == '__main__':
    try:
        dwldir = args.download_dir  # './Data'
        basename = args.basename  # 'gedci_export_dossier_'
        virtual = args.virtual_screen
        logging.getLogger("").setLevel(args.log_level)
        
    except Exception as e:
        raise e

    logging.getLogger("").setLevel('INFO')

    try:
        print(virtual)
        gedciScrapper = GEDCIScrapper(dwldir, basename, display=virtual)
        gedciScrapper.run_download()
    except KeyboardInterrupt as e:
        logging.warning('ERROR: %s.  Exiting' % e)
        gedciScrapper.d.close()
    except Exception as e:
        # comment fermer les stops associés au buy.. on mettre des reduces only
        gedciScrapper.d.close()
        logging.warning('ERROR: %s.  Exiting' % e)

from django.urls import path
from . import views as gedci_views

app_name = 'gedci'

urlpatterns = [
    path('', gedci_views.Accueil.as_view(), name='accueil'),
    path('cdp/<slug:filtre>/<slug:order>', gedci_views.Chefs2Pole.as_view(), name='cdp'),
    path('conseillers/<slug:filtre>/<slug:order>/<slug:acteur>', gedci_views.Conseillers.as_view(), name='conseillers'),
    path('conseillers2/<slug:filtre>/<slug:order>/<slug:acteurid>', gedci_views.Conseillers2.as_view(), name='conseillers2'),
    path('etatsdos/<int:docid>', gedci_views.EtatsDosView.as_view(), name="etatsdos"),
    path('importation/', gedci_views.ImportationView.as_view(), name='importation'),
    path('stats/', gedci_views.Stats.as_view(), name='stats'),
]

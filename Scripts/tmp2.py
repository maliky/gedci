{% extends "myapp/base.html" %}

{% block content %}
<h2>Login</h2>
{% if form.errors %}
<p>Votre login et votre mot de passe ne collent pas.  Essayez encore.</p>
{% endif %}

{% if next %}
{% if user.is_authenticated %}
<p>Ce compte n'a pas accès à cette page.  Essayer avec un autre compte utilisateur.</p>
{% else %}
<p>Vous devez vous authentifier pour voir cette page.</p>
{% endif %}
{% endif %}

<form method="post" action="{% url 'myapp:index' %}">
  {% csrf_token %}
  <table>
    <tr>
      <td>{{ form.username.label_tag }}</td>
      <td>{{ form.username }}</td>
    </tr>
    <tr>
      <td>{{ form.password.label_tag }}</td>
      <td>{{ form.password }}</td>
    </tr>
  </table>

  <input type="submit" value="login">
  <input type="hidden" name="next" value="{{ next }}">
</form>

<!-- {# Assumes you setup the password_reset view in your URLconf #} -->
<!-- <p><a href="{% url 'password_reset' %}">Mot de passe oublié?</a></p> -->

{% endblock %}


# ################
November 15, 2018 - 16:01:20
Django version 2.0.3, using settings 'myProject.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
GET=<QueryDict: {}>
[15/Nov/2018 16:01:23] "GET /myapp/index HTTP/1.1" 302 0
[15/Nov/2018 16:01:23] "GET /myapp/login/ HTTP/1.1" 200 1654
POST=<QueryDict: {'csrfmiddlewaretoken': ['trucmuch'], 'username': ['bleblanc'], 'password': ['cluedo'], 'next': ['']}>
request.user? AnonymousUser
request.user.groups? <QuerySet []>
Internal Server Error: /myapp/index
Traceback (most recent call last):
  File "/home/baz/.local/share/virtualenvs/Python--sNkv1kf/lib/python3.6/site-packages/django/core/handlers/exception.py", line 35, in inner
    response = get_response(request)
  File "/home/baz/.local/share/virtualenvs/Python--sNkv1kf/lib/python3.6/site-packages/django/core/handlers/base.py", line 139, in _get_response
    "returned None instead." % (callback.__module__, view_name)
ValueError: The view myapp.views.Index didn't return an HttpResponse object. It returned None instead.
[15/Nov/2018 16:01:27] "POST /myapp/index HTTP/1.1" 500 64384



# stack trace 

ValueError at /myapp/home

The view myapp.views.Home didn't return an HttpResponse object. It returned None instead.

Request Method: 	POST
Request URL: 	http://localhost:8000/myapp/home
Django Version: 	2.0.3
Exception Type: 	ValueError
Exception Value: 	

The view myapp.views.Home didn't return an HttpResponse object. It returned None instead.

Exception Location: 	/home/baz/.local/share/virtualenvs/Python--sNkv1kf/lib/python3.6/site-packages/django/core/handlers/base.py in _get_response, line 139
Python Executable: 	/home/baz/.local/share/virtualenvs/Python--sNkv1kf/bin/python
Python Version: 	3.6.4
Python Path: 	

['/home/baz/Formation/Prog/Python/websites/myProject',
 '/home/baz/.local/share/virtualenvs/Python--sNkv1kf/lib/python36.zip',
 '/home/baz/.local/share/virtualenvs/Python--sNkv1kf/lib/python3.6',
 '/home/baz/.local/share/virtualenvs/Python--sNkv1kf/lib/python3.6/lib-dynload',
 '/usr/local/lib/python3.6',
 '/home/baz/.local/share/virtualenvs/Python--sNkv1kf/lib/python3.6/site-packages',
 '/home/baz/.local/share/virtualenvs/Python--sNkv1kf/lib/python3.6/site-packages/yowsup2-2.5.7-py3.6.egg',
 '/home/baz/.local/share/virtualenvs/Python--sNkv1kf/lib/python3.6/site-packages']

Server time: 	jeu, 15 Nov 2018 15:41:54 +0000


# ################ ./myapp/models.py ################

from django.shortcuts import render,redirect
from django.contrib.auth.models import Group
from myapp.models import UserMYAPP
from django.views import View


class UserMYAPP(AbstractUser):
    pass

# ...

# ################ ./myapp/view.py ################

class Home(View):

    def get(self, request):
        print("GET=%s" % request.GET)
        return redirect('/myapp/login/')
    
    def post(self, request):
        print("POST=%s" % request.POST)
        # here the user should be authentificated
        
        print("request.user? %s" % request.user)
        print("request.user.groups? %s" % request.user.groups.all())

        if len(request.user.groups.filter(name__in=['groupA', 'groupB'])):
            return redirect('/myapp/lettergp')

        elif len(request.user.groups.filter(name__in=['group1', 'group2'])):
            return redirect('/myapp/numbergp')

# ...

# ################ myapp/urls.py ################

from django.urls import path
from django.contrib.auth import views as auth_views
from . import views as myapp_views

app_name = 'myapp'

urlpatterns = [
    path('home', myapp_views.Home.as_view(), name='home'),
    path('lettergp/', myapp_views.LetterGroup.as_view(), name='lettergp'),
    path('numbergp/', myapp_views.NumberGroup.as_view(), name='numbergp'),

    path('login/', auth_.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
]

# ################ myProject/urls.py ################

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.views.generic import RedirectView

urlpatterns = [
    path('myapp/', include('myapp.urls')),
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),

   # les redirects
    path('accounts/login/', RedirectView.as_view(url='/myapp/login/')),
    path('accounts/profile/', RedirectView.as_view(url='/myapp/home/')),
    path('accounts/logout/', RedirectView.as_view(url='/myapp/logout')), 
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


# ################ myProject/settings.py ################
# ...

INSTALLED_APPS = [
    'myapp.apps.MyappConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

# ...
ROOT_URLCONF = 'myProject.urls'

# ...

# Redirection
LOGIN_REDIRECT_URL = 'myapp/'

AUTH_USER_MODEL = 'myapp.UserMYAPP'

# ################ myapp/admin.py ################

from django.contrib import admin
from myapp.models import UserMYAPP

admin.site.register(UserMYAPP)

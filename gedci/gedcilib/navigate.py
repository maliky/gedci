from .mlksoup import beauty, get_linked_table
import logging
from time import sleep

MTP = "kmK#96047"
EMAIL = "conseiller12campusfrance@institutfrancais.ci"
BURL_THREAD = "https://pastel.diplomatie.gouv.fr/etudesenfrance/dyn/public/authentification/login.html"


def find_elt_by(func, d, text):
    """Soit un type func parmis LINK, CSS ou ID, un driver et un text correspondant à func.  Appel la fonction selenium correspondant à func.
    C'est juste un raccourci et harmonisation de notation"""
    if func == 'LINK':
        elt = d.find_element_by_link_text(text)
    elif func == 'CSS':
        elt = d.find_element_by_css_selector(text)
    elif func == 'ID':
        elt = d.find_element_by_id(text)
    return elt


def logging_CF(d, time=60, headless=False):
    print("Logging in")
    d.get(BURL_THREAD)
    find_elt_by('CSS', d, '[placeholder="Identifiant"]').clear()
    find_elt_by('CSS', d, '[placeholder="Identifiant"]').send_keys(EMAIL)
    find_elt_by('CSS', d, '[placeholder="Mot de passe"]').clear()
    find_elt_by('CSS', d, '[placeholder="Mot de passe"]').send_keys(MTP)
    find_elt_by('CSS', d, '[type="submit"]').click()
    print("done")


def page_fichiers_generes(d):
    """Aller à la page des fichiers générés"""
    find_elt_by('ID', d, 'menu.cefFichiersGeneres').click()


def page_recherche(d):
    # va sur la page de recherche étudiants
    find_elt_by('ID', d, 'menu.dossiersEtudiant').click()


def attend_fin_generation(d, stable):
    """patiente sur la page des fichiers générés que la dernière génération soit effective. Si c'est bon renvoi le tableau déjà formaté"""
    lktab = get_linked_table(stable, alinks=[5])
    tps = 10
    while lktab.where(lktab.demandePar == 'KONE Malik').dropna().etat.iloc[0] != 'Généré':
        logging.info(f"waiting {tps}s")
        sleep(tps)
        tps = (tps * 2) if tps < 3600 else max(tps, 3600)
        page_fichiers_generes(d)
        sleep(5)
        stable = beauty(d).table
        lktab = get_linked_table(stable, alinks=[5])
    return lktab

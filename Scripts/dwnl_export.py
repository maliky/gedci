import logging
from time import sleep
from gedci.gedcilib import forms, navigate, scraptools, mlksoup, arguments

dwldir =  './Data'
basename =  'gedci_export_dossier_'
display = False
logging.getLogger("").setLevel('INFO')

# initialisation du driver
try:
    if display:
        from pyvirtualdisplay import Display
        display = Display(visible=0, size=(1024, 900), use_xauth=True)
        display.start()
        logging.info('running with virtualdispaly')
    d = scraptools.init_wd_firefox(2, dwldir=dwldir, headless=False, proxy=False)
    logging.info('Init of driver done')
except Exception:
    raise
logging.info('waiting 10s')
sleep(10)

# on se log
logging.info('Logging to cF ?')
navigate.logging_CF(d, dwldir=dwldir)
logging.info('Done. Waiting 10s')
sleep(10)

logging.info('Navigate à la page de recherche ?')
navigate.page_recherche(d)
logging.info('Done. waiting 10s')
sleep(10)

# do_defaut_filling
logging.info('Remplissage du formulaire ?')
forms.remplir_formulaire_recherche(d, default=True)
logging.info('Done. waiting 15s')
sleep(5)

# lancer la recherche différée
logging.info('lancement recherche differee ?')
forms.lance_recherche_differee(d, basename=basename)
logging.info('Done. waiting 1min')
sleep(10)

logging.info('Aller à la page des fichiers générés ?')
navigate.page_fichiers_generes(d)
logging.info('Done. wainting 10s')
sleep(10)

logging.info('Patiente fin du de la génération')
stable = mlksoup.beauty(d).table
tabdf = navigate.attend_fin_generation(d, stable)


tabdf = scraptools.telecharge_fichier_genere_malik(d, tabdf=tabdf, ieme=0)
logging.info(f'Done.  Fichier {dwldir}/{basename}.xlsx enregistré')
sleep(10)


stable = mlksoup.beauty(d).table
scraptools.supprimer_fichiers_generes_malik(d, stable,ieme="*")

logging.info('driver closed ?')
d.close()
logging.info('Done.')

# #### Script ####


# voir get arguments from command ligne in kola
args = arguments.get_args()



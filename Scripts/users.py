users = {'jrose': {'email': 'jrose@gedci.app', 'password': 'LeSilence',
                   'first_name': 'Joséphine', 'last_name': 'Rose'},
         'mmoutarde': {'email': 'mmoutarde@gedci.app', 'password': 'LeSilence',
                       'first_name': 'Michael', 'last_name': 'Moutarde'},
         'bleblanc': {'email': 'bleblanc@gedci.app', 'password': 'LeSilence',
                      'first_name': 'Blanche', 'last_name': 'Leblanc'},
         'jolive': {'email': 'jolive@gedci.app', 'password': 'LeSilence',
                    'first_name': 'John', 'last_name': 'Olive'},
         'ppervenche': {'email': 'ppervenche@gedci.app', 'password': 'LeSilence',
                        'first_name': 'Patricia', 'last_name': 'Pervenche'},
         'pviolet': {'email': 'pviolet@gedci.app', 'password': 'LeSilence',
                     'first_name': 'Peter', 'last_name': 'Violet'}}

groups = {'jrose': ['chefs_de_poles'],
          'mmoutarde': ['chefs_de_poles'],
          'bleblanc': ['interviewers', 'chefs_de_poles'],
          'jolive': ['interviewers'],
          'ppervenche': ['interviewers', 'correcteurs'],
          'pviolet': ['correcteurs']}

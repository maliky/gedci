# -*- coding: utf-8 -*-
import argparse


def get_scrapping_args():
    parser = argparse.ArgumentParser(
        description="Choisir le basename, le dwldir et s'il faut démarrer avec ou sans écran virtuel")

    parser.add_argument('-d', '--download_dir', type=str, default='~/Downloads',
                        help="dossier de téléchargement (default ./Data)")
    parser.add_argument('-b', '--basename', type=str, default='gedci_export_',
                        help='nom de base au quel sera ajouté l\'extension appropriée, (default gedci_export_)')
    parser.add_argument('-v', '--virtual_screen', action="store_true",
                        help="si présent utilise un écran virtuel sinon non")
    parser.add_argument('-l', '--log_level', type=str, default='INFO',
                        help='set the log level (default INFO)')

    # parsing arguments from command ligne
    return parser.parse_args()


def get_wrangle_args():
    parser = argparse.ArgumentParser(
        description="récupère le fichier xls téléchargé et le transforme en csv.")

    parser.add_argument('-d', '--download_dir', type=str, default='./Data',
                        help="dossier de téléchargement (default du navigateur)")
    parser.add_argument('-b', '--file_name', type=str,
                        help='nom du fichier à wrangle')
    parser.add_argument('-l', '--log_level', type=str, default='INFO',
                        help='set the log level (default INFO)')

    # parsing arguments from command ligne
    return parser.parse_args()


def get_populate_args():
    parser = argparse.ArgumentParser(
        description="Choisir le basename, le dwldir et s'il faut démarrer avec ou sans écran virtuel")

    parser.add_argument('-d', '--download_dir', type=str, default='./Data',
                        help="dossier de téléchargement (default ./Data)")
    parser.add_argument('-b', '--basename', type=str,
                        help='nom de base au quel sera ajouté l\'extension appropriée, (default gedci_export_)')
    parser.add_argument('-l', '--log_level', type=str, default='INFO',
                        help='set the log level (default INFO)')

    # parsing arguments from command ligne
    return parser.parse_args()
